<?php

namespace App\Http\Controllers\website;

use App\Models\Show;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShowsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(app()->isLocale('ar')) {
            $shows=Show::where('title_ar','<>',null)->where('content_ar','<>',null)->with('images')->get();

        }else{
            $shows=Show::where('title_en','<>',null)->where('content_en','<>',null)->with('images')->get();

        }
        return view('website.shows',compact('shows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show=Show::find($id);
        $artist_id=$show->artist_id;
        if(app()->isLocale('ar')) {
            $related=Show::where('artist_id',$artist_id)->where('title_ar','<>',null)->where('content_ar','<>',null)->get();
        }else{
            $related=Show::where('artist_id',$artist_id)->where('title_en','<>',null)->where('content_en','<>',null)->get();

        }
        return view('website.show_details',compact('show','related'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
