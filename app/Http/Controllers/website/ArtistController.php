<?php

namespace App\Http\Controllers\website;

use App\Models\Artist;
use App\Models\ArtistEventType;
use App\Models\ArtistMedia;
use App\Models\Country;
use App\Models\Currency;
use App\Models\EntertainmentSubcategory;
use App\Models\Reservation;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artists=Artist::where('status','accepted')->get();
        return view('website.artists',compact('artists'));
    }
    public function entertainments($id){
        $subcategories=EntertainmentSubcategory::where('category_id',$id)->pluck('id')->toArray();
        $artists=Artist::where('status','accepted')->whereIn('entertainment_id',$subcategories)->get();
        return view('website.artists',compact('artists'));

    }
    public function event_type($id){
        $artists=Artist::where('status','accepted')->whereHas('eventType',function ($q)use($id){
            $q->where('event_type_id',$id);

        })->get();
        return view('website.artists',compact('artists'));

    }
    public function entertainment_regional(){
        $artists=Artist::where('status','accepted')->whereHas('city',function ($q){
            $q->whereHas('country',function ($c){
                $c->where('type',1);

            });

        })->get();
        return view('website.artists',compact('artists'));

    }
    public function entertainment_global(){
        $artists=Artist::where('status','accepted')->whereHas('city',function ($q){
            $q->whereHas('country',function ($c){
                $c->where('type',2);

            });

        })->get();
        return view('website.artists',compact('artists'));

    }
    public function entertainment_saudi(){
        $artists=Artist::where('status','accepted')->whereHas('city',function ($q){
            $q->whereHas('country',function ($c){
                $c->where('type',3);

            });

        })->get();
        return view('website.artists',compact('artists'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries=Country::all();
        return view('website.artist_register',compact('countries'));
    }
    public function create_reservation($id){
        $countries=Country::all();
        $artist=Artist::find($id);
        $currencies=Currency::all();

        return view('website.reservation',compact('artist','countries','currencies'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_reservation(Request $request){
        if (app()->isLocale('ar'))
        {
        $request->validate([
            'f_name' => 'required|regex:/\p{Arabic}/u',
            's_name' => 'required|regex:/\p{Arabic}/u',
            'email' => 'required|email',
            'phone' => 'required',
            'service_id' => 'required',
            'currency_id' => 'required',
            'city_id' => 'required',
            'event_name' => 'required|regex:/\p{Arabic}/u',
            'address' => 'required|regex:/\p{Arabic}/u',
            'budget' => 'required',
            'date' => 'required',

        ],[
            'f_name.regex'=>'من فضلك أدخل باللغة العربية',
            's_name.regex'=>'من فضلك أدخل باللغة العربية',
            'event_name.regex'=>'من فضلك أدخل باللغة العربية',
            'address.regex'=>'من فضلك أدخل باللغة العربية'
        ]);
        }else{
            $request->validate([
                'f_name' => 'required|regex:/^[a-zA-Z0-9 ]+$/',
                's_name' => 'required|regex:/^[a-zA-Z0-9 ]+$/',
                'email' => 'required|email',
                'phone' => 'required',
                'service_id' => 'required',
                'currency_id' => 'required',
                'city_id' => 'required',
                'event_name' => 'required|regex:/^[a-zA-Z0-9 ]+$/',
                'address' => 'required|regex:/^[a-zA-Z0-9 ]+$/',
                'budget' => 'required',
                'date' => 'required',

            ],[
                'f_name.regex'=>'Please Enter In English',
                's_name.regex'=>'Please Enter In English',
                'event_name.regex'=>'Please Enter In English',
                'address.regex'=>'Please Enter In English'
            ]);

        }
        $data=$request->except(['country']);
        $reservation=Reservation::create($data);
        return redirect('/')->with('success',trans('site.reservation_success'));

    }
    public function store(Request $request)
    {
        if (app()->isLocale('ar'))
        {
        $request->validate([
            'name' => 'required|regex:/\p{Arabic}/u',
            'email' => 'required|email',
            'contact_number' => 'required',
            'quick_response_number' => 'required',
            'gender' => 'required',
            'nationality' => 'required|regex:/\p{Arabic}/u',
            'show_name' => 'required|regex:/\p{Arabic}/u',
            'description' => 'required|regex:/\p{Arabic}/u',
            'required_equipment' => 'required|regex:/\p{Arabic}/u',
            'duration' => 'required|regex:/\p{Arabic}/u',
            'city_id' => 'required',
            'event_type_ids' => 'required',
            'entertainment_id' => 'required',
            'main_image'=>'required|file|mimes:jpeg,png,jpg,gif,pdf'

        ],[
            'name.regex'=>'من فضلك أدخل باللغة العربية',
            'nationality.regex'=>'من فضلك أدخل باللغة العربية',
            'show_name.regex'=>'من فضلك أدخل باللغة العربية',
            'description.regex'=>'من فضلك أدخل باللغة العربية',
            'required_equipment.regex'=>'من فضلك أدخل باللغة العربية',
            'duration.regex'=>'من فضلك أدخل باللغة العربية',
        ]);
        }else{
            $request->validate([
                'name' => 'required|regex:/^[a-zA-Z0-9 ]+$/',
                'email' => 'required|email',
                'contact_number' => 'required',
                'quick_response_number' => 'required',
                'gender' => 'required',
                'nationality' => 'required|regex:/^[a-zA-Z0-9 ]+$/',
                'show_name' => 'required|regex:/^[a-zA-Z0-9 ]+$/',
                'description' => 'required|regex:/^[a-zA-Z0-9 ]+$/',
                'required_equipment' => 'required|regex:/^[a-zA-Z0-9 ]+$/',
                'duration' => 'required|regex:/^[a-zA-Z0-9 ]+$/',
                'city_id' => 'required',
                'event_type_ids' => 'required',
                'entertainment_id' => 'required',
                'main_image'=>'required|file|mimes:jpeg,png,jpg,gif,pdf'


            ],[
                'name.regex'=>'Please Enter In English',
                'nationality.regex'=>'Please Enter In English',
                'show_name.regex'=>'Please Enter In English',
                'description.regex'=>'Please Enter In English',
                'required_equipment.regex'=>'Please Enter In English',
                'duration.regex'=>'Please Enter In English',
            ]);

        }

        $data=$request->except(['images','main_image','country','terms','event_type_ids']);
        $artist=Artist::create($data);
        if ($request->event_type_ids){
            foreach ($request->event_type_ids as $id){
                ArtistEventType::create([
                    'event_type_id'=>$id,
                    'artist_id'=>$artist->id
                ]);
            }

        }
        if ($request->main_image){
            $imageName = 'artist-' . time() . '-' . uniqid() . '.' . $request->main_image->getClientOriginalExtension();
            $extension=$request->main_image->extension();
            $request->main_image->move(
                base_path() . '/public/uploads/artists/', $imageName
            );
            $artist->main_image='/uploads/artists/'.$imageName;
        }
        //images and videoes
        if ($request->hasFile('images'))
        {
            $this->validate($request,
                [
                    'images.*'=>'file|mimes:jpeg,png,jpg,gif,pdf,mp4'
                ]);
            $files=$request->file('images');
            foreach ($files as $file)
            {
                $imageName = 'artist-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
                $extension=$file->extension();
                $file->move(
                    base_path() . '/public/uploads/artists/', $imageName
                );

                ArtistMedia::create([
                    'media' => '/uploads/artists/' . $imageName,
                    'type' =>$extension,
                    'artist_id' => $artist->id,
                ]);
            }

        }

        return redirect('/')->with('success',trans('site.artist_register_success'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $artist=Artist::where('id',$id)->with('images')->first();
        $related=Artist::where('entertainment_id',$artist->entertainment_id)->get();
        $main_image=ArtistMedia::where('artist_id',$id)->where('type','<>','mp4')->first();
        return view('website.show_artist',compact('artist','main_image','related'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
