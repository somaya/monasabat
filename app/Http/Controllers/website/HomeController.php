<?php

namespace App\Http\Controllers\website;

use App\Models\Artist;
use App\Models\Event;
use App\Models\News;
use App\Models\Setting;
use App\Models\Show;
use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Excel;
use File;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('add-patient');
    }

    public function import(Request $request){
        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required|file|mimes:xlsx,xls,csv'
        ));

        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();

                if(!empty($data) && $data->count()){

                    foreach ($data as $key => $value) {


                        $insert[] = [
                            'id'=>$value->id?:'',
                            'name_en' => $value->name_en ?:'',
                            'name_ar' => $value->name_ar ?:'',
                            'type' => $value->type ?:'',
//                            'en_lname' => $value->last_name ?:'',
//                            'ar_name' => $value->first_name_s ?:'',
//                            'ar_father_name' => $value->father_name_s ?:'',
//                            'ar_grandfather_name' => $value->mid_name_s ?:'',
//                            'ar_lname' => $value->last_name_s ?:'',
//                            'nationality_number' => $value->national_id ?:'',
//                            'address' => $value->address ?:'',
//                            'phone' => $value->tel_no ?:'',
//                            'medical_number' => $value->patient_id ?:'',
                        ];
                    }

                    if(!empty($insert)){
                        foreach (array_chunk($insert,1000) as $t) {

                            $insertData = DB::table('countries')->insert($t);
                        }
                        if ($insertData) {
                            Session::flash('success', 'Your Data has successfully imported');
                        }else {
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                    }
                }

                return back();

            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }
    }
    public function index()
    {
        if(app()->isLocale('ar')){
            $news=News::where('title_ar','<>',null)->where('content_ar','<>',null)->take(6)->get();
            $artists=Artist::where('status','accepted')->with('images')->latest()->take(6)->get();
            $events=Event::where('title_ar','<>',null)->where('content_ar','<>',null)->with('images')->latest()->take(6)->get();
            $shows=Show::where('title_ar','<>',null)->where('content_ar','<>',null)->with('images')->latest()->take(6)->get();

        }else{
            $news=News::where('title_en','<>',null)->where('content_en','<>',null)->take(6)->get();
            $artists=Artist::where('status','accepted')->with('images')->latest()->take(6)->get();
            $events=Event::where('title_en','<>',null)->where('content_en','<>',null)->with('images')->latest()->take(6)->get();
            $shows=Show::where('title_en','<>',null)->where('content_en','<>',null)->with('images')->latest()->take(6)->get();
        }

        return view('welcome',compact('news','artists','events','shows'));
    }
    public function team(){
        $teams=Team::all();
        return view('website.team',compact('teams'));
    }
    public function aboutus(){

        return view('website.about');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
