<?php

namespace App\Http\Controllers\website;

use App\Models\Event;
use App\Models\EventMedia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(app()->isLocale('ar')) {
            $events=Event::where('title_ar','<>',null)->where('content_ar','<>',null)->with('images')->get();
        }else{
            $events=Event::where('title_en','<>',null)->where('content_en','<>',null)->with('images')->get();
        }

        return view('website.events',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event=Event::where('id',$id)->with('images')->first();
        if(app()->isLocale('ar')) {
            $related = Event::where('title_ar','<>',null)->where('content_ar','<>',null)->with('images')->get();
        }else{
            $related = Event::where('title_en','<>',null)->where('content_en','<>',null)->with('images')->get();

        }
        return view('website.event_details',compact('event','related'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
