<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Event;
use App\Models\Idea;
use App\Models\Reservation;
use App\Models\Show;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events=Event::count();
        $shows=Show::count();
        $reservations=Reservation::count();
        $ideas=Idea::count();
        $pending_artists=Artist::where('status','pending')->count();
        $accepted_artists=Artist::where('status','accepted')->count();

        return view('admin.dashboard',compact('events','shows','pending_artists','reservations','accepted_artists','ideas'));
    }

}
