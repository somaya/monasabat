<?php

namespace App\Http\Controllers\admin;

use App\Models\Artist;
use App\Models\Show;
use App\Models\ShowMedia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShowsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shows=Show::all();
        return view('admin.shows.index',compact('shows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $artists=Artist::where('status','accepted')->get();
        return view('admin.shows.add',compact('artists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'title_ar'=>'required',
//                'title_en'=>'required',
//                'content_en'=>'required',
                'content_ar'=>'required',
                'date'=>'required',
                'artist_id'=>'required',
                'main_image'=>'required|file|mimes:jpeg,png,jpg,gif,mp4',
//                'medias.*'=>'sometimes|file|mimes:jpeg,png,jpg,gif,mp4'
            ]);

        $file = $request->file('main_image');

        $imageName = 'show-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
        $extension = $file->extension();
        $file->move(
            base_path() . '/public/uploads/shows/', $imageName
        );

        $show= Show::create([
            'main_image' => '/uploads/shows/' . $imageName,
            'title_ar' => $request->title_ar,
            'title_en' => $request->title_en,
            'content_en' => $request->content_en,
            'content_ar' => $request->content_ar,
            'date' => $request->date,
            'artist_id' => $request->artist_id,
        ]);
        // other photos and vedioes
        if ($request->hasFile('medias'))
        {
            $this->validate($request,
                [
                    'medias.*'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            $files=$request->file('medias');
            foreach ($files as $file)
            {
                $imageName = 'show-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
                $extension=$file->extension();
                $file->move(
                    base_path() . '/public/uploads/shows/', $imageName
                );

                ShowMedia::create([
                    'media' => '/uploads/shows/' . $imageName,
                    'type' =>$extension,
                    'show_id' => $show->id,
                ]);
            }

        }


        return redirect('/webadmin/shows')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة العرض بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show=Show::find($id);
        return view('admin.shows.show',compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $show=Show::find($id);
        $artists=Artist::where('status','accepted')->get();

        return view('admin.shows.edit',compact('show','artists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $show=Show::find($id);
        $data=$request->except(['main_image','medias']);
        $show->update($data);

        if ($request->hasFile('medias'))
        {
            $this->validate($request,
                [
                    'medias.*'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            //delete old medias
            ShowMedia::where('show_id',$id)->delete();
            $files=$request->file('medias');
            foreach ($files as $file)
            {
                $imageName = 'show-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
                $extension=$file->extension();
                $file->move(
                    base_path() . '/public/uploads/shows/', $imageName
                );

                ShowMedia::create([
                    'media' => '/uploads/shows/' . $imageName,
                    'type' =>$extension,
                    'show_id' => $show->id,
                ]);
            }

        }
        if ($request->hasFile('main_image'))
        {
            $this->validate($request,
                [
                    'main_image'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            $file=$request->file('main_image');

            $imageName = 'show-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $extension=$file->extension();
            $file->move(
                base_path() . '/public/uploads/shows/', $imageName
            );
            $show->update(['main_image'=>'/uploads/shows/'.$imageName]);

        }
        return redirect('/webadmin/shows')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل العرض بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $show=Show::find($id);
        $medias= ShowMedia::where('show_id',$id)->get();
        foreach ($medias as $media) {
            $old_file =$media->media;
            if (is_file($old_file)) unlink($old_file);
            $media->delete();
        }
        if (is_file($show->main_image)) unlink($show->main_image);

        $show->delete();
        return redirect('/webadmin/shows')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف العرض بنجاح']));
    }
}
