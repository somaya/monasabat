<?php

namespace App\Http\Controllers\admin;

use App\Models\Setting;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting=Setting::first();
        return view('admin.settings.index',compact('setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting=Setting::find($id);
        return view('admin.settings.edit',compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'email'=>'required',
                'phone'=>'required',
                'address'=>'required',
                'aboutus_ar'=>'required',
                'aboutus_en'=>'required',
                'artist_registeration_description_ar'=>'required',
                'artist_registeration_description_en'=>'required',
                'news_description_ar'=>'required',
                'news_description_en'=>'required',
                'new_artists_description_ar'=>'required',
                'new_artists_description_en'=>'required',
                'last_events_description_ar'=>'required',
                'last_events_description_en'=>'required',
                'popular_shows_description_ar'=>'required',
                'popular_shows_description_en'=>'required',
                'footer_description_ar'=>'required',
                'footer_description_en'=>'required',
//                'artist_registeration_description_en'=>'required',
            ]);
        $setting=Setting::find($id);
        $data=$request->except(['logo','expressive_image']);
        $setting->update($data);

        if ($request->hasFile('logo'))
        {
            $this->validate($request,
                [
                    'logo'=>'file|mimes:jpeg,png,jpg,gif'
                ]);
            $file=$request->file('logo');

            $imageName = 'logo-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $extension=$file->extension();
            $file->move(
                base_path() . '/public/uploads/settings/', $imageName
            );
            $setting->update(['logo'=>'/uploads/settings/'.$imageName]);

        }
        if ($request->hasFile('expressive_image'))
        {
            $this->validate($request,
                [
                    'expressive_image'=>'file|mimes:jpeg,png,jpg,gif'
                ]);
            $file=$request->file('expressive_image');

            $imageName = 'expressive_image-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $extension=$file->extension();
            $file->move(
                base_path() . '/public/uploads/settings/', $imageName
            );
            $setting->update(['expressive_image'=>'/uploads/settings/'.$imageName]);

        }
        return redirect('/webadmin/settings')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل الاعدادات بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
