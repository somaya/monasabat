<?php

namespace App\Http\Controllers\admin;

use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news=News::all();
        return view('admin.news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
//                'title_ar'=>'required',
//                'title_en'=>'required',
//                'content_en'=>'required',
//                'content_ar'=>'required',
                'image'=>'required|file|mimes:jpeg,png,jpg,gif,mp4',
            ]);

        $file = $request->file('image');

        $imageName = 'news-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
        $extension = $file->extension();
        $file->move(
            base_path() . '/public/uploads/news/', $imageName
        );

        $new= News::create([
            'image' => '/uploads/news/' . $imageName,
            'title_ar' => $request->title_ar,
            'title_en' => $request->title_en,
            'content_en' => $request->content_en,
            'content_ar' => $request->content_ar,
            'user_id' => auth()->user()->id,
        ]);



        return redirect('/webadmin/news')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة الخبر بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $new=News::find($id);
        return view('admin.news.show',compact('new'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $new=News::find($id);

        return view('admin.news.edit',compact('new'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $new=News::find($id);
        $data=$request->except(['image']);
        $new->update($data);

        if ($request->hasFile('image'))
        {
            $this->validate($request,
                [
                    'image'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            $file=$request->file('image');

            $imageName = 'news-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $extension=$file->extension();
            $file->move(
                base_path() . '/public/uploads/news/', $imageName
            );
            $new->update(['image'=>'/uploads/news/'.$imageName]);

        }
        return redirect('/webadmin/news')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل الخبر بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $new=News::find($id);

        if (is_file($new->image)) unlink($new->image);

        $new->delete();
        return redirect('/webadmin/news')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الخبر بنجاح']));
    }
}
