<?php

namespace App\Http\Controllers\admin;

use App\Models\EventType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventTypes = EventType::all();
        return view('admin.eventTypes.index', compact('eventTypes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.eventTypes.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name_ar'=>'required',
//                'name_en'=>'required',
            ]);

        EventType::create([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,

        ]);

        return redirect('/webadmin/eventTypes')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة تصنيف العروض بنجاح']));



    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventType = EventType::find($id);
        return view('admin.eventTypes.edit', compact( 'eventType'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $eventType =EventType::find($id);
        $data=$request->all();
        $eventType->update($data);
        return redirect('/webadmin/eventTypes')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل تصنيف العروض بنجاح']));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EventType::destroy($id);
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }
}
