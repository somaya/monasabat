<?php

namespace App\Http\Controllers\admin;

use App\Models\Artist;
use App\Models\ArtistEventType;
use App\Models\ArtistMedia;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ArtistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pendingArtists()
    {
        $artists=Artist::where('status','pending')->get();
        return view('admin.artists.pending-artists',compact('artists'));
    }
    public function acceptedArtists()
    {
        $artists=Artist::where('status','accepted')->get();
        return view('admin.artists.accepted-artists',compact('artists'));
    }
    public function accept($id){

        $artist=Artist::find($id);
        $artist->update([
            'status'=>'accepted'
        ]);
        $message='تم قبول تسجيلك لدينا';
        $data = [
            'answer' => $message,

        ];


        Mail::send('emails.reply', $data, function ($message) use ($artist) {
            $message->from('smtp@monasabat.com', 'resetpassword@monasabat')
                ->to($artist->email)
                ->subject('ِAccept Your Registeration, monasabat App');
        });
        return redirect('/webadmin/artist/accepted')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم قبول الفنان بنجاح']));

    }
    public function reject($id){
        $artist=Artist::find($id);
        $artist->update([
            'status'=>'rejected'
        ]);
        $message='تم رفض تسجيلك لدينا';
        $data = [
            'answer' => $message,

        ];


        Mail::send('emails.reply', $data, function ($message) use ($artist) {
            $message->from('smtp@monasabat.com', 'resetpassword@monasabat')
                ->to($artist->email)
                ->subject('Reject Your Registeration, monasabat App');
        });
        return redirect()->back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم رفض الفنان بنجاح']));

    }
    public function sendEmail(){
        return view('admin.artists.sendemail');
    }
    public function storeEmail(Request $request){
        $this->validate($request,
            [
                'message'=>'required',

            ]);
        $emails=Artist::where('status','accepted')->pluck('email')->toArray();
        $data = [
            'answer' => $request->message,

        ];
        foreach ($emails as $email){
            Mail::send('emails.reply', $data, function ($message) use ($email) {
                $message->from('smtp@monasabat.com', 'resetpassword@monasabat')
                    ->to($email)
                    ->subject('Message For All Artists, monasabat App');
            });

        }



        return redirect('/webadmin/artist/accepted')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم ارسال البريد الالكترونى بنجاح']));
    }
    public function artistOfEventType($id){
        $artists=Artist::where('status','accepted')->whereHas('eventType',function ($q)use($id){
            $q->where('event_type_id',$id);

        })->get();
        return view('admin.eventTypes.artists',compact('artists'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries=Country::all();
        return view('admin.artists.add',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|regex:^[\u0621-\u064A0-9 ]+$',
            'email' => 'required|email',
            'contact_number' => 'required',
            'quick_response_number' => 'required',
            'gender' => 'required',
            'nationality' => 'required',
            'show_name' => 'required',
            'description' => 'required',
            'required_equipment' => 'required',
            'duration' => 'required',
            'city_id' => 'required',
            'country' => 'required',
            'event_type_ids' => 'required',
            'entertainment_id' => 'required',
            'entertainment_category' => 'required',
            'main_image' => 'required|file|mimes:jpeg,png,jpg,gif,mp4',


        ]);
        $data = $request->except(['main_image', 'medias', 'country', 'entertainment_category','event_type_ids']);
        $artist = Artist::create($data);
        if ($request->event_type_ids){
            foreach ($request->event_type_ids as $id){
                ArtistEventType::create([
                    'event_type_id'=>$id,
                    'artist_id'=>$artist->id
                ]);
            }

        }
        if ($request->hasFile('main_image')){
            $file = $request->file('main_image');

        $imageName = 'artist-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
        $extension = $file->extension();
        $file->move(
            base_path() . '/public/uploads/artists/', $imageName
        );
            $artist->update([
                'main_image'=>'/uploads/artists/' . $imageName,
                'status'=>'accepted'

            ]);
    }


        //images and videoes
        if ($request->hasFile('medias'))
        {
            $this->validate($request,
                [
                    'medias.*'=>'file|mimes:jpeg,png,jpg,gif,pdf,mp4'
                ]);
            $files=$request->file('medias');
            foreach ($files as $file)
            {
                $imageName = 'artist-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
                $extension=$file->extension();
                $file->move(
                    base_path() . '/public/uploads/artists/', $imageName
                );

                ArtistMedia::create([
                    'media' => '/uploads/artists/' . $imageName,
                    'type' =>$extension,
                    'artist_id' => $artist->id,
                ]);
            }

        }

        return redirect('/webadmin/artist/accepted')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة الفنان بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $artist=Artist::find($id);
        return view('admin.artists.show',compact('artist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries=Country::all();
        $artist=Artist::find($id);
        return view('admin.artists.edit',compact('countries','artist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'contact_number' => 'required',
            'quick_response_number' => 'required',
            'gender' => 'required',
            'nationality' => 'required',
            'show_name' => 'required',
            'description' => 'required',
            'required_equipment' => 'required',
            'duration' => 'required',
            'city_id' => 'required',
            'country' => 'required',
            'event_type_ids' => 'required',
            'entertainment_id' => 'required',
            'entertainment_category' => 'required',


        ]);

        $artist=Artist::find($id);
        $data=$request->except(['main_image','medias', 'country', 'entertainment_category','event_type_ids']);
        $artist->update($data);
        if ($request->event_type_ids){
            ArtistEventType::where('artist_id',$id)->delete();
            foreach ($request->event_type_ids as $id){
                ArtistEventType::create([
                    'event_type_id'=>$id,
                    'artist_id'=>$artist->id
                ]);
            }

        }

        if ($request->hasFile('main_image')){
            $this->validate($request,
                [
                    'main_image'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            $file = $request->file('main_image');

            $imageName = 'artist-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $extension = $file->extension();
            $file->move(
                base_path() . '/public/uploads/artists/', $imageName
            );
            $artist->update([
                'main_image'=>'/uploads/artists/' . $imageName,
                'status'=>'accepted'

            ]);
        }


        //images and videoes
        if ($request->hasFile('medias'))
        {
            $this->validate($request,
                [
                    'medias.*'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            //delete old medias
            ArtistMedia::where('artist_id',$id)->delete();
            $files=$request->file('medias');
            foreach ($files as $file)
            {
                $imageName = 'artist-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
                $extension=$file->extension();
                $file->move(
                    base_path() . '/public/uploads/artists/', $imageName
                );

                ArtistMedia::create([
                    'media' => '/uploads/artists/' . $imageName,
                    'type' =>$extension,
                    'artist_id' => $artist->id,
                ]);
            }

        }

        return redirect('/webadmin/artist/accepted')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل الفنان بنجاح']));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artist=Artist::find($id);
        $medias= ArtistMedia::where('artist_id',$id)->get();
        ArtistEventType::where('artist_id',$id)->delete();
        foreach ($medias as $media) {
            $old_file =$media->media;
            if (is_file($old_file)) unlink($old_file);
            $media->delete();
        }
        if (is_file($artist->main_image)) unlink($artist->main_image);

        $artist->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الفنان بنجاح']));

    }
}
