<?php

namespace App\Http\Controllers\admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $users=User::all();
        return view('admin.users.index',compact('users'));
    }

    public function create(){
        return view('admin.users.add');

    }

    public function store(Request $request){
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);
        $user=new User();

        $user->name =$request->name;
        $user->email = $request->email;
        $user->role=1;
        $user->password = bcrypt($request->password);

        $user->save();
        return redirect('/webadmin/users')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تمت الاضافة بنجاح']));
    }

    public function edit($id){
        $user=User::find($id);

        return view('admin.users.edit',compact('user'));

    }

    public function update($id,Request $request){


        $user=User::find($id);
        if ($request->name!=$user->name) {
            $user->name = $request->name;
        }
        if ($request->email!=$user->email)
        {
            $this->validate($request,[
                'email' => 'required|email|max:255|unique:users',
            ]);

            $user->email = $request->email;

        }
        if ($request->password){
        $user->password =bcrypt($request->password);
        }


        $user->save();
        return redirect('/webadmin/users')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم التعديل بنجاح']));
    }

    public function show($id)
    {


    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $user->delete();
        return redirect('/webadmin/users')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }
}
