<?php

namespace App\Http\Controllers\admin;

use App\Models\Entertainment;
use App\Models\EntertainmentSubcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EntertainmentTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types=Entertainment::all();
        return view('admin.entertainmentTypes.index',compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.entertainmentTypes.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name_ar'=>'required',
//                'name_en'=>'required',
            ]);

        Entertainment::create([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,

        ]);

        return redirect('/webadmin/entertainments-types')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة نوع الترفيه بنجاح']));



    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type=Entertainment::find($id);
        return view('admin.entertainmentTypes.edit', compact('type'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type=Entertainment::find($id);
        $data=$request->all();
        $type->update($data);
        return redirect('/webadmin/entertainments-types')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل نوع الترفيه بنجاح']));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type=Entertainment::find($id);
        $subtypes=EntertainmentSubcategory::where('category_id',$id)->delete();
        $type->delete();
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }
}
