<?php

namespace App\Http\Controllers\admin;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services=Service::all();
        return view('admin.services.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'title_ar'=>'required',
//                'title_en'=>'required',
//                'description_en'=>'required',
                'description_ar'=>'required',
                'image'=>'required|file|mimes:jpeg,png,jpg,gif,mp4',
            ]);

        $file = $request->file('image');

        $imageName = 'service-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
        $extension = $file->extension();
        $file->move(
            base_path() . '/public/uploads/services/', $imageName
        );

        $service= Service::create([
            'image' => '/uploads/services/' . $imageName,
            'title_ar' => $request->title_ar,
            'title_en' => $request->title_en,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'user_id' => auth()->user()->id,
        ]);



        return redirect('/webadmin/services')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة الخدمة بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service=Service::find($id);
        return view('admin.services.show',compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service=Service::find($id);

        return view('admin.services.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service=Service::find($id);
        $data=$request->except(['image']);
        $service->update($data);

        if ($request->hasFile('image'))
        {
            $this->validate($request,
                [
                    'image'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            $file=$request->file('image');

            $imageName = 'service-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $extension=$file->extension();
            $file->move(
                base_path() . '/public/uploads/services/', $imageName
            );
            $service->update(['image'=>'/uploads/services/'.$imageName]);

        }
        return redirect('/webadmin/services')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل الخدمه بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service=Service::find($id);

        if (is_file($service->image)) unlink($service->image);

        $service->delete();
        return redirect('/webadmin/services')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الخدمه بنجاح']));
    }
}
