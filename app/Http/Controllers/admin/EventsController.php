<?php

namespace App\Http\Controllers\admin;

use App\Models\Event;
use App\Models\EventMedia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events=Event::all();
        return view('admin.events.index',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.events.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'title_ar'=>'required',
//                'title_en'=>'required',
//                'content_en'=>'required',
                'content_ar'=>'required',
                'date'=>'required',
                'main_image'=>'required|file|mimes:jpeg,png,jpg,gif,mp4',
//                'medias.*'=>'sometimes|file|mimes:jpeg,png,jpg,gif,mp4'
            ]);

            $file = $request->file('main_image');

            $imageName = 'event-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $extension = $file->extension();
            $file->move(
                base_path() . '/public/uploads/events/', $imageName
            );

           $event= Event::create([
                'main_image' => '/uploads/events/' . $imageName,
                'title_ar' => $request->title_ar,
                'title_en' => $request->title_en,
                'content_en' => $request->content_en,
                'content_ar' => $request->content_ar,
                'date' => $request->date,
            ]);
            // other photos and vedioes
        if ($request->hasFile('medias'))
        {
            $this->validate($request,
                [
                    'medias.*'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            $files=$request->file('medias');
            foreach ($files as $file)
            {
                $imageName = 'event-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
                $extension=$file->extension();
                $file->move(
                    base_path() . '/public/uploads/events/', $imageName
                );

                EventMedia::create([
                    'media' => '/uploads/events/' . $imageName,
                    'type' =>$extension,
                    'event_id' => $event->id,
                ]);
            }

        }


        return redirect('/webadmin/events')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة الفاعليه بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event=Event::find($id);
        return view('admin.events.show',compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event=Event::find($id);
        return view('admin.events.edit',compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event=Event::find($id);
        $data=$request->except(['main_image','medias']);
        $event->update($data);

        if ($request->hasFile('medias'))
        {
            $this->validate($request,
                [
                    'medias.*'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            //delete old medias
            EventMedia::where('event_id',$id)->delete();
            $files=$request->file('medias');
            foreach ($files as $file)
            {
                $imageName = 'event-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
                $extension=$file->extension();
                $file->move(
                    base_path() . '/public/uploads/events/', $imageName
                );

                EventMedia::create([
                    'media' => '/uploads/events/' . $imageName,
                    'type' =>$extension,
                    'event_id' => $event->id,
                ]);
            }

        }
        if ($request->hasFile('main_image'))
        {
            $this->validate($request,
                [
                    'main_image'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            $file=$request->file('main_image');

                $imageName = 'event-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
                $extension=$file->extension();
                $file->move(
                    base_path() . '/public/uploads/events/', $imageName
                );
                $event->update(['main_image'=>'/uploads/events/'.$imageName]);

        }
        return redirect('/webadmin/events')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل الفاعليه بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event=Event::find($id);
        $medias= EventMedia::where('event_id',$id)->get();
        foreach ($medias as $media) {
            $old_file =$media->media;
            if (is_file($old_file)) unlink($old_file);
            $media->delete();
        }
        if (is_file($event->main_image)) unlink($event->main_image);

        $event->delete();
        return redirect('/webadmin/events')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الفاعلية بنجاح']));
    }
}
