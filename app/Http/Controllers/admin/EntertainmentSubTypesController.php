<?php

namespace App\Http\Controllers\admin;

use App\Models\Entertainment;
use App\Models\EntertainmentSubcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EntertainmentSubTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subtypes=EntertainmentSubcategory::all();
        return view('admin.entertainmentSubTypes.index',compact('subtypes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types=Entertainment::all();
        return view('admin.entertainmentSubTypes.add',compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name_ar'=>'required',
//                'name_en'=>'required',
                'category_id'=>'required',
            ]);

        EntertainmentSubcategory::create([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'category_id' => $request->category_id,

        ]);

        return redirect('/webadmin/entertainments-subTypes')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة نوع الترفيه الفرعى بنجاح']));



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $types=Entertainment::all();
        $subtype=EntertainmentSubcategory::find($id);
        return view('admin.entertainmentSubTypes.edit',compact('types','subtype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subtype=EntertainmentSubcategory::find($id);
        $data=$request->all();
        $subtype->update($data);
        return redirect('/webadmin/entertainments-subTypes')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل نوع الترفيه الفرعى بنجاح']));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EntertainmentSubcategory::destroy($id);
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }
}
