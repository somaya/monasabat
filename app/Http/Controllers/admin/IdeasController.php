<?php

namespace App\Http\Controllers\admin;

use App\Models\Idea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IdeasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ideas=Idea::all();
        return view('admin.ideas.index',compact('ideas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ideas.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'title_ar'=>'required',
//                'title_en'=>'required',
//                'description_en'=>'required',
                'description_ar'=>'required',
                'image'=>'required|file|mimes:jpeg,png,jpg,gif,mp4',
            ]);

        $file = $request->file('image');

        $imageName = 'idea-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
        $extension = $file->extension();
        $file->move(
            base_path() . '/public/uploads/ideas/', $imageName
        );

        $idea= Idea::create([
            'image' => '/uploads/ideas/' . $imageName,
            'title_ar' => $request->title_ar,
            'title_en' => $request->title_en,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'user_id' => auth()->user()->id,
        ]);



        return redirect('/webadmin/ideas')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة العرض المميز بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $idea=Idea::find($id);
        return view('admin.ideas.show',compact('idea'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idea=Idea::find($id);

        return view('admin.ideas.edit',compact('idea'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $idea=Idea::find($id);
        $data=$request->except(['image']);
        $idea->update($data);

        if ($request->hasFile('image'))
        {
            $this->validate($request,
                [
                    'image'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            $file=$request->file('image');

            $imageName = 'idea-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $extension=$file->extension();
            $file->move(
                base_path() . '/public/uploads/ideas/', $imageName
            );
            $idea->update(['image'=>'/uploads/ideas/'.$imageName]);

        }
        return redirect('/webadmin/ideas')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل العرض المميز بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idea=Idea::find($id);

        if (is_file($idea->image)) unlink($idea->image);

        $idea->delete();
        return redirect('/webadmin/ideas')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف العرض المميز بنجاح']));
    }
}
