<?php

namespace App\Http\Controllers\admin;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts=Contact::all();
        return view('admin.contacts.index',compact('contacts'));
    }
    public function reply($id){
        $contact=Contact::find($id);
        return view('admin.contacts.reply',compact('contact'));

    }
    public function sendReply(Request $request,$id){
        $this->validate($request,
            [
                'message'=>'required',

            ]);
        $contact=Contact::find($id);
        $data = [
            'answer' => $request->message,

        ];


        Mail::send('emails.reply', $data, function ($message) use ($contact) {
            $message->from('smtp@monasabat.com', 'resetpassword@monasabat')
                ->to($contact->email)
                ->subject('Reply For Your Message, monasabat App');
        });
        return redirect('/webadmin/contacts')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الرد بنجاح']));



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact=Contact::find($id);
        return view('admin.contacts.show',compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contact::destroy($id);
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));


    }
}
