<?php

namespace App\Http\Controllers\admin;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider=Slider::all();
        return view('admin.sliders.index',compact('slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [

                'image'=>'required|file|mimes:jpeg,png,jpg,gif',
            ]);
        if ($request->hasFile('image')) {

            $file = $request->file('image');

            $imageName = 'slider-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $extension = $file->extension();
            $file->move(
                base_path() . '/public/uploads/sliders/', $imageName
            );

            Slider::create([
                'image' => '/uploads/sliders/' . $imageName,

            ]);
        }



        return redirect('/webadmin/sliders')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة الصوره بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slider=Slider::find($id);
        return view('admin.sliders.show',compact('slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider=Slider::find($id);

        return view('admin.sliders.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider=Slider::find($id);


        if ($request->hasFile('image'))
        {
            $this->validate($request,
                [
                    'image'=>'file|mimes:jpeg,png,jpg,gif,mp4'
                ]);
            $file=$request->file('image');

            $imageName = 'slider-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $extension=$file->extension();
            $file->move(
                base_path() . '/public/uploads/sliders/', $imageName
            );
            $slider->update(['image'=>'/uploads/sliders/'.$imageName]);

        }
        return redirect('/webadmin/sliders')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل الصوره بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider=Slider::find($id);

        if (is_file($slider->image)) unlink($slider->image);

        $slider->delete();
        return redirect('/webadmin/sliders')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الصوره بنجاح']));
    }
}
