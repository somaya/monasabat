<?php

namespace App\Http\Controllers\admin;

use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams=Team::all();
        return view('admin.teams.index',compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.teams.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name'=>'required',
                'job'=>'required',
                'photo'=>'required|file|mimes:jpeg,png,jpg,gif',
            ]);

        $file = $request->file('photo');

        $imageName = 'team-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
        $extension = $file->extension();
        $file->move(
            base_path() . '/public/uploads/teams/', $imageName
        );

         Team::create([
            'photo' => '/uploads/teams/' . $imageName,
            'name' => $request->name,
            'job' => $request->job,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'insta' => $request->insta,
            'linkedin' => $request->linkedin,
        ]);



        return redirect('/webadmin/teams')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة العضو بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team=Team::find($id);
        return view('admin.teams.show',compact('team'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team=Team::find($id);
        return view('admin.teams.edit',compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $team=Team::find($id);
        $data=$request->except(['photo']);
        $team->update($data);

        if ($request->hasFile('photo'))
        {
            $this->validate($request,
                [
                    'photo'=>'file|mimes:jpeg,png,jpg,gif'
                ]);
            $file=$request->file('photo');

            $imageName = 'team-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $extension=$file->extension();
            $file->move(
                base_path() . '/public/uploads/teams/', $imageName
            );
            $team->update(['photo'=>'/uploads/teams/'.$imageName]);

        }
        return redirect('/webadmin/teams')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل العضو بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team=Team::find($id);

        if (is_file($team->photo)) unlink($team->photo);

        $team->delete();
        return redirect('/webadmin/teams')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف العضو بنجاح']));
    }
}
