<?php

namespace App\Providers;

use App\Models\Entertainment;
use App\Models\EventType;
use App\Models\Idea;
use App\Models\News;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Slider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (app()->isLocale('ar')){
            View::share([
                'settings'=>Setting::first(),
                'ideas'=>Idea::where('title_ar','<>',null)->where('description_ar','<>',null)->get(),
                'news'=>News::where('title_ar','<>',null)->where('content_ar','<>',null)->latest()->get(),
                'event_types'=>EventType::where('name_ar','<>',null)->get(),
                'entertainments'=>Entertainment::all(),
                'services'=>Service::where('title_ar','<>',null)->where('description_ar','<>',null)->get(),
                'sliders'=>Slider::all()
            ]);

        }else{
            View::share([
                'settings'=>Setting::first(),
                'ideas'=>Idea::where('title_en','<>',null)->where('description_en','<>',null)->get(),
                'news'=>News::where('title_en','<>',null)->where('content_en','<>',null)->latest()->get(),
                'event_types'=>EventType::where('name_en','<>',null)->get(),
                'entertainments'=>Entertainment::all(),
                'services'=>Service::where('title_en','<>',null)->where('description_en','<>',null)->get(),
                'sliders'=>Slider::all()
            ]);
        }
    }
}
