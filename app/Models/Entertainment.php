<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entertainment extends Model
{
    protected $table='entertainment_categories';
    protected $guarded=[];
}
