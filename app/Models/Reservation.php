<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table='reservations';
    protected $guarded=[];
    public function service(){
        return $this->belongsTo(Service::class,'service_id');
    }
    public function city(){
        return $this->belongsTo(City::class,'city_id');
    }
    public function currency(){
        return $this->belongsTo(Currency::class,'currency_id');
    }
    public function artist(){
        return $this->belongsTo(Artist::class,'artist_id');
    }
}
