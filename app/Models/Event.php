<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table='events';
    protected $guarded=[];
    public function images(){
        return $this->hasMany(EventMedia::class,'event_id');
    }
}
