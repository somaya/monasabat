<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    protected $table='ideas';
    protected $guarded=[];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
