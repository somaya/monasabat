<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    protected $table='shows';
    protected $guarded=[];
    public function images(){
        return $this->hasMany(ShowMedia::class,'show_id');
    }
    public function artist(){
        return $this->belongsTo(Artist::class,'artist_id');
    }
}
