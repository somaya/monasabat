<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntertainmentSubcategory extends Model
{
    protected $table='entertainment_subcategories';
    protected $guarded=[];
    public function entertainment()
    {
        return $this->belongsTo(Entertainment::class, 'category_id');
    }

}
