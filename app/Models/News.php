<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table='news';
    protected $guarded=[];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
