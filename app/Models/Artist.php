<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $table='artists';
    protected $guarded=[];
    public function images(){
        return $this->hasMany(ArtistMedia::class,'artist_id');
    }
    public function city(){
        return $this->belongsTo(City::class,'city_id');
    }
    public function entertainment()
    {
        return $this->belongsTo(EntertainmentSubcategory::class, 'entertainment_id');
    }
    public function eventType()
    {
        return $this->belongsToMany(EventType::class, 'artist_event_type','artist_id','event_type_id');    }

}
