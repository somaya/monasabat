<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistEventType extends Model
{
    protected $table='artist_event_type';
    protected $guarded=[];
}
