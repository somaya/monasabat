<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShowMedia extends Model
{
    protected $table='show_medias';
    protected $guarded=[];
}
