<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistMedia extends Model
{
    protected $table='artist_medias';
    protected $guarded=[];
}
