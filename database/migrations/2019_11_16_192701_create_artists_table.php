<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('main_image');
            $table->string('contact_number');
            $table->string('quick_response_number');
            $table->tinyInteger('gender');// 1 for male , 2 for female
            $table->string('nationality');
            $table->text('description');
            $table->text('required_equipment');
            $table->string('duration');
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('insta')->nullable();
            $table->string('snapchat')->nullable();
            $table->string('pintrest')->nullable();
            $table->enum('status',['pending','accepted','rejected']);

            $table->unsignedInteger('city_id')->nullable();
            $table->foreign('city_id')
                ->references('id')->on('cities');

            $table->unsignedInteger('event_type_id')->nullable();
            $table->foreign('event_type_id')
                ->references('id')->on('event_types');

            $table->unsignedInteger('entertainment_id')->nullable();
            $table->foreign('entertainment_id')
                ->references('id')->on('entertainment_subcategories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
}
