<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('f_name');
            $table->string('s_name');
            $table->string('phone');
            $table->string('email');
            $table->string('event_name');
            $table->string('date');
            $table->float('budget');
            $table->string('address');
            $table->text('comments');

            $table->unsignedInteger('artist_id')->nullable();
            $table->foreign('artist_id')
                ->references('id')->on('artists');

            $table->unsignedInteger('city_id')->nullable();
            $table->foreign('city_id')
                ->references('id')->on('cities');

            $table->unsignedInteger('currency_id')->nullable();
            $table->foreign('currency_id')
                ->references('id')->on('currencies');

            $table->unsignedInteger('service_id')->nullable();
            $table->foreign('service_id')
                ->references('id')->on('services');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
