<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('show_medias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('media');
            $table->string('type');
            $table->unsignedInteger('show_id')->nullable();
            $table->foreign('show_id')
                ->references('id')->on('shows');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('show_medias');
    }
}
