<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Input;

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'website\HomeController@index');
Route::get('/aboutus', 'website\HomeController@aboutus');
Route::resource('/services', 'website\ServicesController');
Route::resource('/ideas', 'website\IdeasController');
Route::resource('/news', 'website\NewsController');
Route::resource('/artists', 'website\ArtistController');
Route::resource('/contacts', 'website\ContactsController');
Route::get('/entertainment/{id}', 'website\ArtistController@entertainments');
Route::get('/entertainments/event_type/{id}', 'website\ArtistController@event_type');
Route::get('/entertainments/regional', 'website\ArtistController@entertainment_regional');
Route::get('/entertainments/global', 'website\ArtistController@entertainment_global');
Route::get('/entertainments/saudi', 'website\ArtistController@entertainment_saudi');
Route::resource('/events', 'website\EventsController');
Route::resource('/shows', 'website\ShowsController');
Route::resource('/reservations', 'website\ReservationsController');
Route::get('/reservation/{id}', 'website\ArtistController@create_reservation');
Route::post('/storereservation', 'website\ArtistController@store_reservation');
Route::get('/changeLang/{locale}', 'website\LocalizationController@index');
Route::get('/team', 'website\HomeController@team');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/information/create/ajax-state', function () {
    $state_id = Input::get('state_id');
    $cities = \App\Models\City::where('country_id', '=', $state_id)->get();
    return json_encode($cities);

});
Route::get('/information/create/ajax-entertainment', function () {
    $entertainment_id = Input::get('entertainment_id');
    $subcategories = \App\Models\EntertainmentSubcategory::where('category_id', '=', $entertainment_id)->get();
    return json_encode($subcategories);


});
Route::get('/excelInsertMedicines', 'website\HomeController@add')->name('index');
Route::post('import', 'website\HomeController@import')->name('import');
Route::group(['prefix' => 'webadmin', 'middleware' => ['webadmin']], function () {

    Route::get('/dashboard', 'HomeController@index');

    Route::get('/logout', 'Auth\LoginController@logout');
    Route::resource('/events', 'admin\EventsController');
    Route::resource('/shows', 'admin\ShowsController');
    Route::resource('/artists', 'admin\ArtistsController');
    Route::resource('/reservations', 'admin\ReservationsController');
    Route::resource('/ideas', 'admin\IdeasController');
    Route::resource('/services', 'admin\ServicesController');
    Route::resource('/news', 'admin\NewsController');
    Route::resource('/currencies', 'admin\CurrenciesController');
    Route::resource('/countries', 'admin\CountriesController');
    Route::resource('/eventTypes', 'admin\EventTypesController');
    Route::resource('/cities', 'admin\CitiesController');
    Route::resource('/contacts', 'admin\ContactsController');
    Route::resource('/teams', 'admin\TeamsController');
    Route::resource('/sliders', 'admin\SlidersController');

    Route::resource('/settings', 'admin\SettingsController');
    Route::resource('/entertainments-types', 'admin\EntertainmentTypesController');
    Route::resource('/entertainments-subTypes', 'admin\EntertainmentSubTypesController');
    Route::get('/artist/pending', 'admin\ArtistsController@pendingArtists');
    Route::get('/artists/event-type/{id}', 'admin\ArtistsController@artistOfEventType');
    Route::get('/artist/accepted', 'admin\ArtistsController@acceptedArtists');
    Route::get('/artist/sendEmail', 'admin\ArtistsController@sendEmail');
    Route::post('/artist/storeEmail', 'admin\ArtistsController@storeEmail');
    Route::get('/artist/{id}/accept', 'admin\ArtistsController@accept');
    Route::get('/artist/{id}/reject', 'admin\ArtistsController@reject');
    Route::get('/contact/{id}/reply', 'admin\ContactsController@reply');
    Route::post('/contact/{id}/sendreply', 'admin\ContactsController@sendReply');
     Route::resource('/users', 'admin\UsersController');



});
