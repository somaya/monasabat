//confirm-alert
$(document).on('click', '.confirm-alert', function (e) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })

  swalWithBootstrapButtons.fire({
    title: 'هل أنت متأكد؟',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'نعم',
    cancelButtonText: 'لا',
  }).then((result) => {
    if (result.value) {
      swalWithBootstrapButtons.fire({
        title: 'تم إضافته للمفضلة بنجاح',
        showConfirmButton: false,
        timer: 1000
      });
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons.fire({
        title: 'تم إلإلغاء',
        showConfirmButton: false,
        timer: 1000
      });

    }
  })

});

//confirm-copy
$(document).on('click', '.confirm-copy', function (e) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })

  swalWithBootstrapButtons.fire({
    title: 'نسخ المنتج',
    text: 'هل انت متأكد من نسخ المنتج سيتم انشاء منتج بنفس البيانات ؟',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'نعم',
    cancelButtonText: 'لا',
  }).then((result) => {
    if (result.value) {
      swalWithBootstrapButtons.fire({
        title: 'تم تكرار المنتج بنجاح',
        showConfirmButton: false,
        timer: 1000
      });
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons.fire({
        title: 'تم إلإلغاء',
        showConfirmButton: false,
        timer: 1000
      });

    }
  })

});






//remove-alert
$(document).on('click', '.remove-alert', function (e) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })

  swalWithBootstrapButtons.fire({
    title: 'هل أنت متأكد؟',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'نعم',
    cancelButtonText: 'لا',
  }).then((result) => {
    if (result.value) {
      swalWithBootstrapButtons.fire({
        title: 'تم الحذف  بنجاح',
        showConfirmButton: false,
        timer: 1000
      });
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons.fire({
        title: 'تم إلإلغاء',
        showConfirmButton: false,
        timer: 1000
      });

    }
  })

});


//ban-alert
$(document).on('click', '.ban-alert', function (e) {
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
  })

  swalWithBootstrapButtons.fire({
    title: 'هل أنت متأكد؟',
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'نعم',
    cancelButtonText: 'لا',
  }).then((result) => {
    if (result.value) {
      swalWithBootstrapButtons.fire({
        title: 'تم الحظر  بنجاح',
        showConfirmButton: false,
        timer: 1000
      });
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons.fire({
        title: 'تم إلإلغاء',
        showConfirmButton: false,
        timer: 1000
      });

    }
  })

});