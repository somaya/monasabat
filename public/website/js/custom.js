//wow
var wow = new WOW({
    offset: 100
});
wow.init();


// language
$(".language-title").click(function () {
    $(this).next("ul").slideToggle();

});


var $winl = $(window); // or $box parent container
var $boxl = $(".main-language");
$winl.on("click.Bst", function (event) {
    if (
        $boxl.has(event.target).length === 0 && //checks if descendants of $box was clicked
        !$boxl.is(event.target) //checks if the $box itself was clicked
    ) {
        $(".main-language ul").slideUp();


    }
});


// menu
$(".menu-icon").click(function () {
    $(this).addClass("active");
    setTimeout(function () {
        $("nav").addClass("active")
    }, 500);
    $(".navbar-section").fadeIn();
    $("body").css({"overflow":"hidden","height":"100%"});

});


$(document).ready(function () {
    // run test on initial page load
    checkSize();

    // run test on resize of the window
    $(window).resize(checkSize);
});

function checkSize() {
    if ($(window).width() < 576) {

        $(function () {
            var $winm = $(window); // or $box parent container
            var $boxm = $("nav,.menu-icon");
            $winm.on("click.Bst", function (event) {
                if (
                    $boxm.has(event.target).length === 0 && //checks if descendants of $box was clicked
                    !$boxm.is(event.target) //checks if the $box itself was clicked
                ) {
                    $(".menu-icon").removeClass("active");
                    setTimeout(function () {
                        $("nav").removeClass("active")
                    }, 0);
                    setTimeout(function () {
                        $(".navbar-section").fadeOut();
                    }, 300);
                    $("body").css({"overflow":"inherit","height":"auto"});


                }
            });
        });
    } else {}
}



//img src change
$(".converted-img").each(function (i, elem) {
    var img_conv = $(elem);
    img_conv.parent(".full-width-img").css({
        backgroundImage: "url(" + img_conv.attr("src") + ")"
    });
});






//form validtion
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();