
$(document).ready(function () {

    $('#state').on('change', function (e) {
        var state_id = e.target.value;
        var lang=$('#state').data('lang');
        console.log("stateid=" + state_id,lang);
        if (state_id) {

            $.ajax({
                url: '/information/create/ajax-state?state_id=' + state_id,
                type: "GET",

                dataType: "json",

                success: function (data) {
                    $('#city_id').empty();
                    if (lang=='ar') {

                        $('#city_id').append('<option value="">اختر المدينة</option>');
                        $.each(data, function (i, city) {


                            $('#city_id').append('<option value="' + city.id + '">' + city.name_ar + '</option>');

                        });
                    }else{
                        $('#city_id').append('<option value="">Select City</option>');
                        $.each(data, function (i, city) {


                            $('#city_id').append('<option value="' + city.id + '">' + city.name_en + '</option>');

                        });

                    }


                }


            });
        } else {
            $('#city_id').empty();
        }
    });
    $('#entertainment_category').on('change', function (e) {
        var entertainment_id = e.target.value;
        var lang=$('#state').data('lang');
        console.log("entertainmentid=" + entertainment_id,lang);
        if (entertainment_id) {

            $.ajax({
                url: '/information/create/ajax-entertainment?entertainment_id=' + entertainment_id,
                type: "GET",

                dataType: "json",

                success: function (data) {
                    $('#entertainment_subcategory').empty();
                    if (lang=='ar') {

                        $('#entertainment_subcategory').append('<option value="">اختر </option>');
                        $.each(data, function (i, subcategory) {


                            $('#entertainment_subcategory').append('<option value="' + subcategory.id + '">' + subcategory.name_ar + '</option>');

                        });
                    }else{
                        $('#entertainment_subcategory').append('<option value="">Select </option>');
                        $.each(data, function (i, subcategory) {
                            if (subcategory.name_en!=null) {


                                $('#entertainment_subcategory').append('<option value="' + subcategory.id + '">' + subcategory.name_en + '</option>');
                            }else{
                                $('#entertainment_subcategory').append('<option value="' + subcategory.id + '">' + subcategory.name_ar + '</option>');


                            }

                        });

                    }


                }


            });
        } else {
            $('#city_id').empty();
        }
    });




});
