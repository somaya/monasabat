<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'users' => 'المستخدمين',
    'create_user'=>'اضافه مستخدم',
    'name'=>'الاسم',
    'email'=>'الايميل',
    'created_date'=>'تاريخ الانشاء',
    'action_taken'=>'الاجراء المتخذ',
    'password'=>'كلمه السر',
    'confirm_password'=>'تاكيد كلمه السر',
    'save'=>'حفظ',
    'doctors'=>'الاطباء',
    'job'=>'الوظيفه',
    'create_doctor'=>'اضافه طبيب',
    'edit_doctor'=>'تعديل طبيب',
    'code'=>'الكود',
    'photo'=>'الصوره',
    'choose_role'=>'اختر مهمه',
    'role'=>'مهمه',

];