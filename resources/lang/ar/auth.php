<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'البيانات خاطئه',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login_title' => 'تسجيل الدخول للوحة التحكم',
    'login_welcome' => 'نظام ارشفة الوثائق',
    'login_welcome_2' => 'مستشفي حوطة بني تميم العام',
    'email' => 'البريد الاكترونى',
    'password' => 'كلمة المرور',
    'remember_me' => 'تذكرنى',
    'forget_password' => 'هل نسيت كلمة السر ؟',
    'login' => 'تسجيل الدخول',
    'control_panel' => 'لوحة التحكم',

];
