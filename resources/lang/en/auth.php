<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login_title' => 'Login To Control Panel',
    'login_welcome' => 'Document archiving system',
    'login_welcome_2' => 'Hota Ibn Tamim General Hospital',
    'email' => 'Email',
    'password' => 'Password',
    'remember_me' => 'Remember me',
    'forget_password' => 'Forget Password?',
    'login' => 'Login',
    'control_panel' => 'Control Panel',

];
