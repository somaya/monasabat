<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'users' => 'users',
    'create_user'=>'create user',
    'name'=>'name',
    'email'=>'email',
    'created_date'=>'date created',
'action_taken'=>'action taken',
    'password'=>'password',
    'confirm_password'=>'confirm password',
    'save'=>'save',
    'doctors'=>'doctors',
    'job'=>'job',
    'create_doctor'=>'Add Doctor',
    'edit_doctor'=>'Edit Doctor',
    'code'=>'code',
    'photo'=>'photo',
    'choose_role'=>'Choose Role',
    'role'=>'Role',
];