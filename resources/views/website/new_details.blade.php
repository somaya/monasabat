@extends("website.layouts.app")
@section('content')
    <section class="news-details-pg margin-bottom-div  black-bg">
        <div class="col-12 text-center first_bg title-of-event">
            @if(app()->isLocale('ar'))
                {{$new->title_ar}}
            @else
                {{$new->title_en }}
            @endif
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8  news-left-grid wow fadeIn margin-bottom-div">
                    <div class="news-img full-width-img  has_seudo">
                        <img src="{{$new->image}}" class="converted-img" alt="logo" />
                    </div>
                    <div class="time-details-list">
                        <ul class="list-inline">
                            <li>
                                <i class="far fa-calendar-alt"></i>
                                {{$new->created_at->toDayDateTimeString()}}
                            </li>

                            <li>
                                <i class="fa fa-user"></i>
                                By:
                                <a href="#" class="first_color"> {{$new->user->name}}</a>
                            </li>

                        </ul>
                    </div>
<p>
                    @if(app()->isLocale('ar'))

                        {{$new->content_ar}}
                    @else
                        {{$new->content_en }}
                    @endif
</p>
                </div>

                <div class="col-xl-4 col-lg-4 news-right-grid wow fadeIn">
                    <div class="latest-news">
                        <h2 class="first_color">{{trans('site.latest_news')}} </h2>

                        <div>
                            <ul class="list-unstyled">
                                @foreach($news->take(4) as $new)
                                    <li>
                                        <a href="/news/{{$new->id}}">
                                            @if(app()->isLocale('ar'))
                                                {{$new->title_ar}}
                                            @else
                                                {{$new->title_en}}
                                            @endif
                                        </a>
                                    </li>
                                @endforeach


                            </ul>
                        </div>

                        <a href="/news" class="custom-btn sm-btn">{{trans('site.show_all')}}</a>
                    </div>
                </div>
            </div>
            <!--end news-grid-->


        </div>
    </section>
@endsection