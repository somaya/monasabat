@extends("website.layouts.app")
@section('content')
    <section class="team-pg margin-div text-center black-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="team-page-header  wow fadeIn full-width-img">
                        <img src="/website/images/main/team-header.png" class="converted-img" alt="team" />
                        <h2>{{app()->isLocale('ar') ? 'فريق العمل':'Our Team'}}</h2>
                    </div>
                </div>
                <!--start page-header-->
                <div class=" page-header margin-div text-center  col-12">
                    <h1 class="first_color wow fadeInUp">{{app()->isLocale('ar') ? 'فريقنا ':'Our Team'}} </h1>
                    <p class="wow fadeIn">
                        @if(app()->isLocale('en'))

                        Our team is trained in accordance with the best rules of entertainment event management starting from planning through the implementation t will be expected with emphasis on adherence to the highest criteria, we partner collaborate with various entities to bring you the best of the best in the entertainment industry.
                        We are  always ready to offer creative solutions for our clients,
                            @else
                            مع خبرة تمتد لأكثر من 10 سنوات ، يدير فريقنا الفعاليات الترفيهية  وفقًا لأحدث  قواعد إدارتها،  بدءًا من التخطيط وحتى التنفيذ ، مع الالتزام بأعلى معايير الجودة  ، يتعامل فريقنا مع مختلف الكيانات الترفيهية،  في اكثر من 16 دولة  لنقدم كل ماهو مميز، وجاهزين دائما لتقديم كل ماهو مبتكر

                        @endif
                    </p>
                </div>
                <!--end page-header-->


                <!--start team-grid-->
                @foreach($teams as $team)
                <div class="col-xl-3 col-lg-4 col-6 team-grid wow fadeIn">
                    <div class="team-div">
                        <figure class="effect-ming">
                            <div class="team-img full-width-img"><img src="{{$team->photo}}" class="converted-img" alt="img08">
                                <figcaption>
                                    <h2>{{$team->name}}</h2>
                                    <div>
                                        <p> {{$team->job}}</p>
                                    </div>
                                </figcaption>
                            </div>
                            <div class="team-information">
                                <h3 class="first_color">{{$team->name}}</h3>
                                <span class="team-job">{{$team->job}}</span>
                                <!--start social-team-->
                                <div class="social-team">
                                    <ul class="list-inline  auto-icon">
                                        @if($team->facebook)
                                        <li>
                                            <a href="{{$team->facebook}}" target="_blank"><i class="fab fa-linkedin"></i></a>
                                        </li>
                                        @endif
                                         @if($team->linkedin)
                                        <li>
                                            <a href="{{$team->linkedin}}" target="_blank"><i class="fab fa-instagram"></i></a>
                                        </li>
                                            @endif
                                         @if($team->insta)

                                        <li>
                                            <a href="{{$team->insta}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                            @endif
                                        @if($team->twitter)
                                        <li>
                                            <a href="{{$team->twitter}}" target="_blank"><i class="fab fa-twitter"></i></a>
                                        </li>
                                            @endif
                                    </ul>
                                </div>
                                <!--end social-team-->
                            </div>

                        </figure>
                    </div>
                </div>
                <!--end team-grid-->
                    @endforeach



            </div>
        </div>
    </section>
    @endsection
