@extends("website.layouts.app")
@section('content')
    <section class="news-details-pg margin-bottom-div  black-bg">
        <div class="col-12 text-center first_bg title-of-event">
            @if(app()->isLocale('ar'))
            {{$service->title_ar}}
            @else
            {{$service->title_en}}
            @endif

        </div>
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8  news-left-grid wow fadeIn margin-bottom-div">
                    <div class="news-img full-width-img  has_seudo">
                        <img src="{{$service->image}}" class="converted-img" alt="logo" />
                    </div>
                    <p>

                    @if(app()->isLocale('ar'))

                    {!!  $service->description_ar!!}
                    @else
                        {!!  $service->description_en !!}
                    @endif
                    </p>

                </div>

                <div class="col-xl-4 col-lg-4 news-right-grid wow fadeIn">
                    <div class="latest-news">
                        <h2 class="first_color">{{trans('site.more_services')}} </h2>

                        <div>
                            <ul class="list-unstyled">
                                @foreach($services->take(4) as $service)
                                <li>
                                    <a href="/services/{{$service->id}}">
                                        @if(app()->isLocale('ar'))
                                            {{$service->title_ar}}
                                        @else
                                            {{$service->title_en}}
                                        @endif                                    </a>
                                </li>
                                @endforeach


                            </ul>
                        </div>

                        <a href="/services" class="custom-btn sm-btn">{{trans('site.show_all')}}</a>
                    </div>
                </div>
            </div>
            <!--end news-grid-->


        </div>
    </section>
    @endsection