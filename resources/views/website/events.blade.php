@extends("website.layouts.app")
@section('content')
    @include('website.layouts.slider')
    <!-- start entertainment-pg
         ================ -->
    <section class="news  entertainment-pg  pageswithslider gray-bg">
        <div class="container">
            <div class="row">


                <!--start news-grid-->
                @foreach($events as $event)
                <div class="news-grid col-xl-3 col-lg-4 col-sm-6 wow fadeIn">
                    <a href="/events/{{$event->id}}">
                        <div class="news-img full-width-img  has_seudo">
                            <img src="{{$event->main_image}}" class="converted-img" alt="logo" />
                            <div class="news-caption">
                                <h3 class="white-text">{{app()->isLocale('ar')? $event->title_ar: $event->title_en}}</h3>
                                <p class="white-text">
                                    @if(app()->isLocale('ar'))
                                    {!! str_limit($event->content_ar, $limit = 150, $end = '...') !!}
                                        @else
                                        {!! str_limit($event->content_en, $limit = 150, $end = '...') !!}
                                    @endif

                                </p>
                            </div>
                            <div class="text-center btn-div">
                                <span class="custom-btn sm-btn">{{trans('site.read_more')}}</span>
                            </div>

                        </div>
                    </a>
                </div>
                @endforeach
                <!--end news-grid-->


            </div>
        </div>
    </section>
    <!--end entertainment-pg-->
    @endsection