@extends("website.layouts.app")
@section('content')
    @include('website.layouts.slider')
    <!-- start entertainment-pg
         ================ -->
    <section class="news  entertainment-pg  pageswithslider gray-bg">
        <div class="container">
            <div class="row">


                <!--start news-grid-->
                @foreach($artists as $artist)
                    <div class="news-grid col-xl-3 col-lg-4 col-sm-6 wow fadeIn">
                        <a href="/artists/{{$artist->id}}">
                            <div class="news-img full-width-img  has_seudo">
                                <img src="{{$artist->main_image}}" class="converted-img" alt="logo" />
                                <div class="news-caption">
                                    <h3 class="white-text">{{$artist->name}}</h3>
                                    <p class="white-text">
                                        {{ str_limit($artist->description, $limit = 150, $end = '...') }}
                                    </p>
                                </div>
                                <div class="text-center btn-div">
                                    <span class="custom-btn sm-btn">{{trans('site.read_more')}}</span>
                                </div>

                            </div>
                        </a>
                    </div>
            @endforeach
            <!--end news-grid-->


            </div>
        </div>
    </section>
    <!--end entertainment-pg-->
@endsection