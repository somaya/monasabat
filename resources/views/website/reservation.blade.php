@extends("website.layouts.app")
@section('content')
    <section class="artist-pg margin-div  black-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="first_color wow fadeInUp">{{trans('site.reservation_form')}}  </h2>
                    <p class="wow fadeIn margin-bottom-div">
                        @if(app()->isLocale('en'))
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            @else
                            لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه ... بروشور او فلاير على سبيل المثال ... او نماذج مواقع انترنت ...
                            لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه ... بروشور او فلاير على سبيل المثال ... او نماذج مواقع انترنت ...
                            لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه ... بروشور او فلاير على سبيل المثال ... او نماذج مواقع انترنت ...
                            @endif
                    </p>

                </div>
                <!--start form-->
                <div class="artist-form-grid red-form has_seudo col-12">
                    <h2 class="first_color wow fadeInUp">{{trans('site.contacts_details')}} </h2>
                    <form class="needs-validation " action="/storereservation" method="post" novalidate>
                        @csrf
                            <input type="hidden" name="artist_id" value="{{isset($artist) ? $artist->id:''}}">
                        <!--start div-->
                        <div class="artist-form white-bg row no-marg-row">
                            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                                <div class="col-md-4">
                                    <label>{{trans('site.first_name')}} *</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" name="f_name" value="{{old('f_name')}}" class="form-control" required>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_name')}}
                                    </div>
                                    @if ($errors->has('f_name'))
                                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('f_name') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                                <div class="col-md-4">
                                    <label>{{trans('site.second_name')}} *</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" name="s_name" value="{{old('s_name')}}" class="form-control" required>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_name')}}
                                    </div>
                                    @if ($errors->has('s_name'))
                                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('s_name') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                                <div class="col-md-4">
                                    <label>{{trans('site.phone')}} *</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="tel" name="phone" value="{{old('phone')}}" class="form-control" required>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_number')}}
                                    </div>
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('phone') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                                <div class="col-md-4">
                                    <label>{{trans('site.email')}} *</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="email" name="email" value="{{old('email')}}" class="form-control" required>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_email')}}
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('email') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                                <div class="col-md-4">
                                    <label>{{trans('site.event_country')}} *</label>
                                </div>
                                <div class="col-md-8">

                                    <select type="text" name="country" class="form-control" id="state" data-lang="{{app()->getLocale()}}" required>
                                        <option value="" disabled selected>{{trans('site.select_country')}}</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{old('country')==$country->id ? 'selected':''}}>{{app()->isLocale('ar') ? $country->name_ar :$country->name_en}}</option>
                                        @endforeach

                                    </select>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_country')}}
                                    </div>
                                    @if ($errors->has('country'))
                                        <span class="help-block">
                                       <strong style="color: red;">{{ $errors->first('country') }}</strong>
                                    </span>
                                    @endif

                                </div>
                            </div>

                            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                                <div class="col-md-4">
                                    <label>{{trans('site.event_city')}} *</label>
                                </div>
                                <div class="col-md-8">
                                    <select type="text" id="city_id"  name="city_id" class="form-control" required>
                                        <option value="" disabled selected>{{trans('site.select_city')}}</option>

                                    </select>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_city')}}
                                    </div>
                                    @if ($errors->has('city_id'))
                                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('city_id') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <!--end div-->


                        <h2 class="first_color wow fadeInUp col-12">{{trans('site.event_details')}} </h2>

                        <!--start div-->
                        <div class="artist-form white-bg row no-marg-row">

                            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                                <div class="col-md-4">
                                    <label>{{trans('site.event_name')}} *</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" name="event_name" value="{{old('event_name')}}" class="form-control" required>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_event_name')}}
                                    </div>
                                    @if ($errors->has('event_name'))
                                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('event_name') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                                <div class="col-md-4">
                                    <label>{{trans('site.event_date')}} *</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" name="date" id="gregDate" value="{{old('date')}}" onclick="pickDate(event)" class="w3-input w3-border" class="form-control" required>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_event_date')}}
                                    </div>
                                    @if ($errors->has('date'))
                                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('date') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row no-marg-row col-12 wow fadeIn">
                                <div class="col-lg-2 col-md-4">
                                    <label>{{trans('site.event_address')}}*</label>
                                </div>
                                <div class="col-lg-10 col-md-8">
                                    <input type="text" name="address" value="{{old('address')}}" class="form-control" required>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_event_address')}}
                                    </div>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('address') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                                <div class="col-md-4">
                                    <label> {{trans('site.event_budget')}}*</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="number" name="budget" value="{{old('budget')}}" class="form-control" required>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_event_budget')}}
                                    </div>
                                    @if ($errors->has('budget'))
                                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('budget') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                                <div class="col-md-4">
                                    <label>{{trans('site.currency')}} *</label>
                                </div>
                                <div class="col-md-8">
                                    <select type="text" name="currency_id" class="form-control" required>
                                        <option value="" disabled selected>{{app()->isLocale('en')? 'please select':'من فضلك اختر'}}</option>
                                        @foreach($currencies as $currency)
                                        <option value="{{$currency->id}}" {{old('currency_id')==$currency->id ? 'selected':''}}>{{app()->isLocale('ar') ? $currency->name_ar :($currency->name_en?:$currency->name_ar)}}</option>
                                        @endforeach

                                    </select>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_currency')}}
                                    </div>
                                    @if ($errors->has('currency_id'))
                                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('currency_id') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row no-marg-row col-12  wow fadeIn">
                                <div class="col-lg-2 col-md-4">
                                    <label>{{trans('site.service_required')}} *</label>
                                </div>
                                <div class="col-lg-10 col-md-8">
                                    <select type="text" name="service_id"  class="form-control" required>
                                        <option value="" disabled selected>{{app()->isLocale('en')? 'please select':'من فضلك اختر'}}</option>
                                        @foreach($services as $service)
                                            <option value="{{$service->id}}" {{old('service_id')==$service->id ? 'selected':''}}>{{app()->isLocale('ar') ? $service->title_ar :($service->title_en?:$service->title_ar)}}</option>

                                        @endforeach


                                    </select>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_event_service')}}
                                    </div>
                                    @if ($errors->has('service_id'))
                                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('service_id') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row no-marg-row col-12  wow fadeIn">
                                <div class="col-lg-2 col-md-4">
                                    <label>{{trans('site.additional_comments')}}</label>
                                </div>
                                <div class="col-lg-10 col-md-8">
                                    <textarea class="form-control" name="comments">{{old('comments')}}</textarea>

                                </div>
                            </div>

                        </div>
                        <!--end div-->


                        <!--start div-->
                        <div class="artist-form white-bg row no-marg-row">


                            <div class="form-group row no-marg-row col-12  wow fadeIn">
                                <div class="col-lg-2 col-md-4">
                                    <label>{{trans('site.captcha')}}</label>
                                </div>
                                <div class="col-lg-10 col-md-8 capatcha-div">
                                    <input type="text" id="UserCaptchaCode" class="form-control CaptchaTxtField" required>
                                    <span id="WrongCaptchaError" class="error"></span>
                                    <div class='CaptchaWrap'>
                                        <div id="CaptchaImageCode" class="CaptchaTxtField">
                                            <canvas id="CapCode" class="capcode" width="230" height="80"></canvas>
                                        </div>
                                        <span class="ReloadBtn">{{trans('site.cant_read_captcha')}} <span click onclick='CreateCaptcha();'> {{trans('site.here')}}</span> </span>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group col-12 margin-top-div two-btns wow fadeIn text-center">
                                <button type="submit" class="custom-btn btnSubmit" onclick="CheckCaptcha();">{{trans('site.reserve')}}</button>
                                <button type="reset" class="custom-btn">{{trans('site.reset')}}</button>

                            </div>

                        </div>
                        <!--end div-->

                    </form>
                </div>
                <!--end form-->
            </div>
        </div>
    </section>

@endsection
