@extends("website.layouts.app")
@section('content')
    @include('website.layouts.slider')

    <section class="news services-pg  pageswithslider  gray-bg">
        <div class="container">
            @foreach($services as $service)
            <div class="row no-marg-row  ideas-grid wow fadeIn">
                <div class="col-xl-5 col-lg-5 col-md-6 news-left-grid">
                    <div class="news-img full-width-img  has_seudo">
                        <img src="{{$service->image}}" class="converted-img" alt="logo" />
                    </div>
                </div>

                <div class="col-xl-7 col-lg-7 col-md-6 news-right-grid">
                    <div class="news-right-grid">
                        <h2 class="first_color">
                            @if(app()->isLocale('ar'))
                                {{$service->title_ar}}
                            @else
                                {{$service->title_en}}
                            @endif
                        </h2>
                        <p class="dark-text">
                            @if(app()->isLocale('ar'))
                                {!! str_limit($service->description_ar, $limit = 350, $end = '...') !!}

                            @else
                                {!! str_limit($service->description_en, $limit = 350, $end = '...') !!}



                                @endif

                        </p>
                        <div class="btn-div">
                            <a href="/services/{{$service->id}}" class="custom-btn sm-btn">{{trans('site.read_more')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach





        </div>
    </section>
    <!--end services-pg-->
    @endsection