@extends("website.layouts.app")
@section('content')
    @include('website.message')
    <section class="artist-pg margin-div  black-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="first_color wow fadeInUp">{{trans('site.artist_registeration_form')}} : </h2>
                    <p class="wow fadeIn margin-bottom-div">
    @if(app()->isLocale('ar'))
    {!! $settings->artist_registeration_description_ar !!}
    @else
    {!! $settings->artist_registeration_description_en ?:$settings->artist_registeration_description_ar !!}
    @endif
                    </p>


</div>
<!--start form-->
<div class="artist-form-grid red-form has_seudo col-12">
    <h2 class="first_color wow fadeInUp">{{trans('site.artist_details')}} </h2>
    <form class="needs-validation " method="post" action="/artists" enctype="multipart/form-data" novalidate>
        @csrf
        <!--start div-->
        <div class="artist-form white-bg row no-marg-row">
            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.name')}} *</label>
                </div>
                <div class="col-md-9">
                    <input type="text" class="form-control" value="{{old('name')}}" name="name" required>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_name')}}


                    </div>
                    @if ($errors->has('name'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('name') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                <div class="col-lg-6 col-md-3">
                    <label>{{trans('site.country_based')}} *</label>
                </div>
                <div class="col-lg-6 col-md-9">
                    <select type="text" name="country" class="form-control" id="state" data-lang="{{app()->getLocale()}}" required>
                        <option value="" disabled selected>{{trans('site.select_country')}}</option>
                        @foreach($countries as $country)
                        <option value="{{$country->id}}" {{old('country')==$country->id ? 'selected':''}}>{{app()->isLocale('ar') ? $country->name_ar :$country->name_en}}</option>
                        @endforeach

                    </select>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_country')}}
                    </div>
                    @if ($errors->has('country'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('country') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group row no-marg-row  col-lg-6 wow fadeIn">
                <div class="col-lg-3 d-lg-block d-none"></div>
                <div class="col-lg-3 col-md-3">
                    <label>{{trans('site.city_based')}} *</label>
                </div>
                <div class="col-lg-6 col-md-9">
                    <select type="text" id="city_id"  name="city_id" class="form-control" required>
                        <option value="" disabled selected>{{trans('site.select_city')}}</option>

                    </select>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_city')}}
                    </div>
                    @if ($errors->has('city_id'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('city_id') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.email')}} *</label>
                </div>
                <div class="col-md-9">
                    <input type="email" name="email" class="form-control" required>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_email')}}
                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('email') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.contact_number')}} *</label>
                </div>
                <div class="col-md-9">
                    <input type="tel" name="contact_number" value="{{old('contact_number')}}" class="form-control" required>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_contact_number')}}

                    </div>
                    @if ($errors->has('contact_number'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('contact_number') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.quick_contact_number')}} *</label>
                </div>
                <div class="col-md-9">
                    <input type="tel" name="quick_response_number" value="{{old('quick_response_number')}}" class="form-control" required>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_response_number')}}
                    </div>
                    @if ($errors->has('quick_response_number'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('quick_response_number') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group row no-marg-row col-lg-6  wow fadeIn">
                <div class="col-lg-6 col-md-3">
                    <label>{{trans('site.gender')}} *</label>
                </div>
                <div class="col-lg-6 col-md-9">
                    <select type="text" name="gender" class="form-control" required>
                        <option value="" disabled selected>{{trans('site.select_gender')}}</option>
                        <option value="1" {{old('gender')==1 ?'selected':''}}>{{trans('site.male')}}</option>
                        <option value="2" {{old('gender')==2 ?'selected':''}}>{{trans('site.female')}}</option>
                    </select>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_gender')}}
                    </div>
                    @if ($errors->has('gender'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('gender') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group row no-marg-row  col-lg-6 wow fadeIn">
                <div class="col-lg-3 d-lg-block d-none"></div>
                <div class="col-lg-3 col-md-3">
                    <label>{{trans('site.nationality')}} *</label>
                </div>
                <div class="col-lg-6 col-md-9">
                    <input type="text" name="nationality" value="{{old('nationality')}}" class="form-control" required>

                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_nationality')}}
                    </div>
                    @if ($errors->has('nationality'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('nationality') }}</strong>
                            </span>
                    @endif
                </div>
            </div>


            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.facebook')}} </label>
                </div>
                <div class="col-md-9">
                    <input type="url" name="facebook" value="{{old('facebook')}}" class="form-control" >
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_facebook')}}
                    </div>
                </div>
            </div>
            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.twitter')}} </label>
                </div>
                <div class="col-md-9">
                    <input type="url" name="twitter" value="{{old('twitter')}}" class="form-control" >
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_twitter')}}
                    </div>
                </div>
            </div>
            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.insta')}} </label>
                </div>
                <div class="col-md-9">
                    <input type="url" name="insta" value="{{old('insta')}}" class="form-control" >
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_insta')}}
                    </div>
                </div>
            </div>
            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.snapchat')}} </label>
                </div>
                <div class="col-md-9">
                    <input type="url" name="snapchat" value="{{old('snapchat')}}" class="form-control" >
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_snapchat')}}
                    </div>
                </div>
            </div>

        </div>
        <!--end div-->


        <h2 class="first_color wow fadeInUp col-12">{{trans('site.show_details')}} </h2>

        <!--start div-->

        <div class="artist-form white-bg row no-marg-row">
            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.show_name')}} *</label>
                </div>
                <div class="col-md-9">
                    <input type="text" name="show_name" value="{{old('show_name')}}" class="form-control" required >
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_show_name')}}

                    </div>
                    @if ($errors->has('show_name'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('show_name') }}</strong>
                            </span>
                    @endif
                </div>
            </div>
            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.entertainment_type')}} </label>
                </div>
                <div class="col-md-9">
                    <select type="text" class="form-control" id="entertainment_category" required>
                        <option value="" disabled selected>{{app()->isLocale('en')? 'please select':'من فضلك اختر'}}</option>
                        @foreach($entertainments as $entertainment)
                            <option value="{{$entertainment->id}}">{{app()->isLocale('ar') ? $entertainment->name_ar :($entertainment->name_en?:$entertainment->name_ar)}}</option>
                        @endforeach
                    </select>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_entertainment')}}
                    </div>

                </div>
            </div>
            <div class="form-group row no-marg-row col-lg-12  wow fadeIn">
                <div class=" col-md-3">
                    {{--<label>{{trans('site.country_based')}} *</label>--}}
                </div>
                <div class="col-md-9">
                    <select type="text" name="entertainment_id" class="form-control"  id="entertainment_subcategory" required>


                    </select>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_entertainment')}}

                    </div>
                    @if ($errors->has('entertainment_id'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('entertainment_id') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.event_type')}} </label>
                </div>
                <div class="col-md-9">
                    <div class="multi-checkboxes">
                        @foreach($event_types as $index=> $event_type)
                   
                    <div class="form-check">
                       <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="event_type_ids[]" class="custom-control-input" value="{{$event_type->id}}"
                               id="customControl{{$index}}" >
                        <label class="custom-control-label" for="customControl{{$index}}">
                            {{app()->isLocale('ar') ? $event_type->name_ar :$event_type->name_en}}
                        </label>
                      
                    </div>
                    </div>
                            @endforeach
                        <div class="invalid-feedback">
                            {{trans('site.enter_valid_event_types')}}

                        </div>
                        @if ($errors->has('event_type_ids'))
                            <span class="help-block">
                           <strong style="color: red;">{{ $errors->first('event_type_ids') }}</strong>
                        </span>
                        @endif

                    </div>
                    

                </div>
            </div>

            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.details_show')}} *</label>
                </div>
                <div class="col-md-9">
                    <textarea class="form-control" name="description"  required>{{old('description')}}</textarea>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_description')}}

                    </div>
                    @if ($errors->has('description'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('description') }}</strong>
                            </span>
                    @endif
                </div>
            </div>


            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.required_equipment')}}</label>
                </div>
                <div class="col-md-9">
                    <textarea class="form-control" name="required_equipment"  required>{{old('required_equipment')}}</textarea>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_required_equipment')}}

                    </div>
                    @if ($errors->has('required_equipment'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('required_equipment') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.duration')}} *</label>
                </div>
                <div class="col-md-9">
                    <input type="text" name="duration"
                            class="form-control" required>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_duration')}}
                    </div>
                    @if ($errors->has('duration'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('duration') }}</strong>
                            </span>
                    @endif
                </div>
            </div>
            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.main_image')}} *</label>
                </div>
                <div class="col-md-9 custom-file">
                    <input type="file" name="main_image"  class="form-control" id="input-file" required>
                    <label for="input-file" class="custom-input-label form-control"><span>Choose
                            File</span><b></b></label>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_main_image')}}

                    </div>
                    @if ($errors->has('main_image'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('main_image') }}</strong>
                            </span>
                    @endif
                </div>
            </div>



            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                    <label>{{trans('site.images')}} *</label>
                </div>
                <div class="col-md-9 custom-file">
                    <input type="file" name="images[]" multiple class="form-control" id="input-file" required>
                    <label for="input-file" class="custom-input-label form-control"><span>Choose
                            File</span><b></b></label>
                    <div class="invalid-feedback">
                        {{trans('site.enter_valid_images')}}

                    </div>
                    @if ($errors->has('images.*'))
                        <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('images.*') }}</strong>
                            </span>
                    @endif
                </div>
            </div>




            <div class="form-group row no-marg-row col-12  wow fadeIn">
                <div class="col-md-3">
                </div>
                <div class="col-md-9">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="terms" class="custom-control-input"
                               id="customControlValidation1" required>
                        <label class="custom-control-label" for="customControlValidation1">
                            {{trans('site.permission')}}
                        </label>
                        <div class="invalid-feedback">
                            {{trans('site.enter_valid_permission')}}

                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group col-12 margin-div  wow fadeIn text-center">
                <button class="custom-btn">Submit</button>
            </div>

        </div>
        <!--end div-->


    </form>
</div>
<!--end form-->
</div>
</div>
</section>
@endsection