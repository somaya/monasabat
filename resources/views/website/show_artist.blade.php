@extends("website.layouts.app")
@section('content')
    <!-- start show-details-pg
         ================ -->
    <section class="show-details-pg margin-bottom-div  black-bg">
        <div class="col-12 text-center first_bg title-of-event">
            {{$artist->show_name}}
        </div>
        <div class="container">
            <div class="row">

                <div class="row no-marg-row  ideas-grid wow fadeIn">
                    <div class="col-xl-4 col-lg-4  news-left-grid">
                        @if($artist->main_image)
                        <div class="news-img full-width-img  has_seudo">
                            <img src="{{$artist->main_image}}" class="converted-img" alt="logo" />
                        </div>
                        @endif
                        <a href="/reservation/{{$artist->id}}" class="custom-btn full-width-btn">{{trans('site.reservation')}}</a>
                        <!--start social-grid-->
                        <div class="inline-social">
                            <ul class="list-inline footer-social auto-icon">
                                @if($artist->facebook)
                                <li>
                                    <a href="{{$artist->facebook}}" target="_blank" class="fc-icon"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                @endif
                                @if($artist->insta)
                                <li>
                                    <a href="{{$artist->insta}}" target="_blank" class="inst-icon"><i class="fab fa-instagram"></i></a>
                                </li>
                                    @endif
                                    @if($artist->snapchat)
                                <li>
                                    <a href="{{$artist->snapchat}}" target="_blank" class="snap-icon"><i
                                                class="fab fa-snapchat-ghost"></i></a>
                                </li>
                                    @endif
                                    @if($artist->twitter)

                                <li>
                                    <a href="{{$artist->twitter}}" target="_blank" class="tw-icon"><i class="fab fa-twitter"></i></a>
                                </li>
                                    @endif
                                    @if($artist->pintrest)

                                <li>
                                    <a href="{{$artist->pintrest}}" target="_blank" class="yotube-icon"><i class="fab fa-pinterest"></i></a>
                                </li>
                                        @endif

                            </ul>
                        </div>
                        <!--end social-grid-->
                    </div>

                    <div class="col-xl-8 col-lg-8  news-right-grid">
                        <div class="news-right-grid">
                            <h2 class="first_color">{{trans('site.details')}}</h2>
                            <p class="dark-text">
                               {{$artist->description}}
                            </p>
                            {{--<div class="show-list">--}}
                                {{--<ul class="list-unstyled">--}}
                                    {{--<li>--}}
                                        {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
                <!--end news-grid-->


                <!--start news-grid-->
                @foreach($artist->images as $item)
                    @if($item->type=='mp4')
                        {{--<div class="news-grid col-lg-4 col-sm-6 wow fadeIn auto-icon text-center">--}}
                            {{--<a href="/artist/{{$artist->id}}">--}}
                                {{--<div class="news-img full-width-img  has_seudo">--}}

                                    {{--<video width="320" height="240" controls>--}}
                                        {{--<source src="{{ $item->media }}" type="video/mp4">--}}
                                        {{--Your browser does not support the video tag.--}}
                                    {{--</video>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        <div class="news-grid col-lg-4 col-sm-6 wow fadeIn auto-icon text-center">
                            <a href="{{ $item->media }}" data-thumbnail="{{ $item->media }}"
                               class="html5lightbox" data-group="set-2">
                                <div class="news-img full-width-img  has_seudo">
                                    <img src="{{$artist->main_image}}" class="converted-img" alt="logo" />
                                    <i class="fab fa-youtube vedio-icon"></i>
                                </div>
                            </a>
                        </div>
                    @else
                        <div class="news-grid col-lg-4 col-sm-6 wow fadeIn auto-icon text-center">
                            {{--<a href="/artist/{{$artist->id}}">--}}
                             <a href="{{ $item->media }}" data-thumbnail="{{ $item->media }}"
                               class="html5lightbox" data-group="set-2">
                                <div class="news-img full-width-img  has_seudo">
                                    <img src="{{$item->media}}" class="converted-img" alt="logo" />
                                </div>
                                </a>
                            {{--</a>--}}
                        </div>

                    @endif
                <!--end news-grid-->
                    @endforeach



            </div>
        </div>
    </section>
    <!--end show-details-pg-->


    <!-- start news
         ================ -->
    <section class="news margin-index-div vedios-div black-bg related-shows">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="/artists" class="sec-title first_color wow fadeInUp" data-hover="read more">{{trans('site.related_artist')}}<span>{{trans('site.related_artist')}}</span></a>
                </div>
                @foreach($related as $item)
                    @if($item->id!=$artist->id)
                        <div class="news-grid col-lg-4 col-sm-6 wow fadeIn">
                            <a href="/artists/{{$item->id}}">
                                <div class="news-img full-width-img  has_seudo">
                                    <img src="{{$item->main_image}}" class="converted-img" alt="logo" />
                                    <div class="news-caption">
                                        <h3 class="white-text">{{$item->name}}</h3>
                                        <p class="white-text">
                                            {{ str_limit($item->description, $limit = 150, $end = '...') }}
                                        </p>
                                    </div>
                                    <div class="text-center btn-div">
                                        <span class="custom-btn sm-btn">{{trans('site.read_more')}}</span>
                                    </div>

                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach

            </div>
        </div>
    </section>

    <!--end news-->
@endsection