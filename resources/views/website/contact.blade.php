@extends("website.layouts.app")
@section('content')
    @include('admin.message')
    <!--start page-header-->
    <section class="page-header margin-top-div text-center black-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="first_color wow fadeInUp">{{trans('site.contact_with_us')}}</h1>
                    {{--<p class="wow fadeIn">--}}
                        {{--ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod--}}
                        {{--tempor incididunt ut labore et dolore magna.--}}
                    {{--</p>--}}
                </div>
            </div>
        </div>
    </section>
    <!--end page-header-->


    <!-- start contact-pg
         ================ -->
    <section class="contact-pg margin-div  black-bg">
        <div class="container">
            <div class="row">
                <div class="col-12 row contact-bg">
                    <!--start form-->
                    <div class="contact-form-grid has_seudo col-lg-7">
                        <div class="contact-form">
                            <form class="needs-validation icons-form" method="post" action="/contacts" novalidate>
                                @csrf
                                <div class="form-group wow fadeIn">
                                    <input type="text" name="fullname" value="{{old('fullname')}}" class="form-control" placeholder="{{trans('site.full_name')}}" required>
                                    <i class="far fa-user main-form-icon"></i>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_name')}}
                                    </div>
                                </div>

                                <div class="form-group wow fadeIn">
                                    <input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="{{trans('site.email')}}" required>
                                    <i class="far fa-envelope main-form-icon"></i>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_email')}}
                                    </div>
                                </div>

                                <div class="form-group wow fadeIn">
                                    <input type="tel" name="phone" value="{{old('phone')}}" class="form-control" placeholder="{{trans('site.phone')}}" required>
                                    <i class="fa fa-mobile-alt main-form-icon"></i>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_number')}}
                                    </div>
                                </div>

                                <div class="form-group wow fadeIn">
                                    <textarea class="form-control" name="message" placeholder="{{trans('site.message')}}" required>{{old('message')}}</textarea>
                                    <i class="far fa-comment-dots main-form-icon"></i>
                                    <div class="invalid-feedback">
                                        {{trans('site.enter_valid_message')}}
                                    </div>
                                </div>

                                <div class="form-group  wow fadeIn text-center">
                                    <button class="custom-btn">{{trans('site.send')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--end form-->

                    <!--start contact-social-grid-->
                    <div class="contact-social-grid text-center col-md-5">
                        <div class="contact-social">
                            <ul class="list-unstyled">
                                <li class="wow fadeIn">
                                    <a href="#">
                                        <i class="fa fa-map-marker-alt"></i>
                                        <span>{{$settings->address}}</span>
                                    </a>
                                </li>

                                <li class="wow fadeIn">
                                    <a href="tel:01234567899">
                                        <i class="fa fa-tty"></i>
                                        <span>{{$settings->phone}}</span>
                                    </a>
                                </li>

                                <li class="wow fadeIn">
                                    <a href="mailto:jadara@gmail.com">
                                        <i class="fa fa-at"></i>
                                        <span>{{$settings->email}}</span>
                                    </a>
                                </li>

                                <li class="wow fadeIn">
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                        <span>{{$settings->twitter}}</span>
                                    </a>
                                </li>

                                <li class="wow fadeIn">
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                        <span>{{$settings->facebook}}</span>
                                    </a>
                                </li>


                                <li class="wow fadeIn">
                                    <a href="#">
                                        <i class="fab fa-whatsapp"></i>
                                        <span>{{$settings->phone}}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end contact-social-grid-->
                </div>
            </div>
        </div>
    </section>
    <!--end contact-pg-->
    @endsection