<section class="navbar-section text-dir-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="starbox main-stars">
                    <div class='dot'></div>
                    <div class='dot1'></div>
                    <div class='dot2'></div>
                </div>
                <nav>
                    <div class="responsive-logo">
                        <img src="/website/images/main/logo.png" alt="logo" />
                    </div>
                    <ul class="list-inline main-nav-menu">
                        <li ><a href="/">{{trans('site.home')}}</a></li>
                        <li><a href="/aboutus">{{trans('site.aboutus')}}</a></li>
                        <li><a href="/services">{{trans('site.services')}}</a></li>
                        <li>
                            <a href="/artists">{{trans('site.entertainments')}}</a>
                            <ul class="submenu list-unstyled">
                                @foreach($entertainments as $entertainment)
                                <li><a href="/entertainment/{{$entertainment->id}}">{{app()->isLocale('ar') ? $entertainment->name_ar:$entertainment->name_en}}</a></li>
                                @endforeach

                            </ul>
                        </li>
                        <li>
                            <a href="/artists">{{trans('site.event_types')}}</a>
                            <ul class="submenu list-unstyled">
                                @foreach($event_types as $event_type)
                                <li><a href="/entertainments/event_type/{{$event_type->id}}">{{\Illuminate\Support\Facades\App::isLocale('en') ? $event_type->name_en :$event_type->name_ar}}</a></li>
                                @endforeach

                            </ul>
                        </li>
                        <li>
                            <a href="/contacts/create">{{trans('site.contacts')}}</a>
                            <ul class="submenu list-unstyled">
                                <li><a href="/contacts/create">{{trans('site.contactus')}}</a></li>
                                <li><a href="/reservations/create">{{trans('site.show_booking')}}</a></li>
                                <li><a href="/team">{{trans('site.team')}}</a></li>


                            </ul>
                        </li>
                    </ul>

                    <ul class="list-inline responsive-nav-menu">
                        {{--<li><a href="reservation.html">booking show</a></li>--}}
                        <li><a href="/team">our team</a></li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>