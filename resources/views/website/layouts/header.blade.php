<header><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <div class="container">
        <div class="row">

            <!--start div-->
            <div class="col-lg-5 col-md-4 col-sm-6 col-4 main-language-grid wow fadeIn" data-wow-delay=".3s">
                <a href="#" class="menu-icon"><span></span></a>
                <!--start div-->
                <div class="main-language">
                    <span class="language-title">{{app()->isLocale('ar') ?'عربى':'ENG'}}</span>
                    <ul class="list-unstyled">

                        <li><a href="/changeLang/ar">عربي</a></li>

                        <li><a href="/changeLang/en">ENG</a></li>

                    </ul>
                </div>
                <!--end div-->

                <!--start div-->
                <div class="main-phone">
                    <a href="tel:{{$settings->phone}}"><i class="fa fa-phone-square first_color"></i>{{$settings->phone}}</a>
                </div>
                <!--end div-->
            </div>
            <!--end div-->

            <!--start div-->
            <div class="col-lg-2 col-md-3 col-sm-12 col-8 text-dir-center logo-grid">
                <div class="logo-div  wow fadeIn">
                    <a href="/">
                        <img src="{{$settings->logo}}" alt="logo" />
                    </a>
                </div>
            </div>
            <!--end div-->

            <!--start div-->
            <div class="col-lg-5 col-md-5 col-sm-6 xs-center main-search-grid wow fadeIn" data-wow-delay=".3s">
                <div class="main-search">
                    <form class="needs-validation red-form" method="" action="" novalidate>
                        <div class="form-group">
                            <input class="form-control" placeholder="{{trans('site.search')}}" required />
                            <div class="invalid-feedback">
                                please enter valid text
                            </div>
                            <button type="submit" class="pos-search-btn first_color"><i
                                        class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <!--end div-->


        </div>
    </div>
</header>