<section class="main-slider">
    <div class="container-fluid">
        <div class="row">
            <div id="owl-demo" class="owl-carousel owl-theme main-carousel">
                @foreach($sliders as $slider)
                <div class="item">
                    <div class="slider-img full-width-img">
                        <img src="{{$slider->image}}" class="converted-img" alt="slider img" />
                    </div>

                    <div class="slider-caption">
                        {{--<a href="/news/{{$new->id}}"><h2 class="white-text">--}}
                                {{--@if(app()->isLocale('ar'))--}}
                                    {{--{{$new->title_ar}}--}}
                                {{--@else--}}
                                    {{--{{$new->title_en}}--}}
                                {{--@endif</h2></a>--}}
                        {{--<p class="white-text">--}}
                            {{--@if(app()->isLocale('ar'))--}}

                                {{--{{ str_limit($new->content_ar, $limit = 150, $end = '...') }}--}}
                            {{--@else--}}
                                {{--{{ str_limit($new->content_en, $limit = 150, $end = '...') }}--}}
                            {{--@endif--}}
                        {{--</p>--}}
                    </div>
                </div>
                @endforeach

            </div>

        </div>
    </div>
</section>

<section class="services text-center">
    <div class="container">
        <div class="row">

            <!--start services-grid-->

            <!--end services-grid-->


            <!--start services-grid-->

            <!--end services-grid-->

            <!--start services-grid-->
            <div class="services-grid col-lg-3 col-6">
                <div class="services-div">
                    <a href="/entertainments/saudi">
                            <span class="service-icon has_seudo auto-icon">
                                <img src="/website/images/main/cultures.png" alt="img"/>
                            </span>
                        <h3 class="white-text">{{trans('site.saudi_entertainments')}}</h3>
                    </a>
                </div>
            </div>
            <!--end services-grid-->
            <div class="services-grid col-lg-3 col-6">
                <div class="services-div">
                    <a href="/entertainments/regional">
                              <span class="service-icon has_seudo auto-icon">
                               <img src="/website/images/main/earth.png" alt="img"/>
                            </span>
                        <h3 class="white-text">{{trans('site.regional_entertainments')}}</h3>
                    </a>
                </div>
            </div>
            <div class="services-grid col-lg-3 col-6">
                <div class="services-div">
                    <a href="/entertainments/global">
                            <span class="service-icon has_seudo auto-icon">
                                <i class="fa fa-globe-europe"></i>
                            </span>
                        <h3 class="white-text">{{trans('site.global_entertainments')}}</h3>
                    </a>
                </div>
            </div>


            <!--start services-grid-->
            <div class="services-grid col-lg-3 col-6">
                <div class="services-div">
                    <a href="/artists/create">
                            <span class="service-icon has_seudo auto-icon">
                                <i class="fa fa-link"></i>
                            </span>
                        <h3 class="white-text">{{trans('site.artist_Registeration')}}</h3>
                    </a>
                </div>
            </div>
            <!--end services-grid-->

        </div>
    </div>
</section>