<footer class="wow fadeIn">
    <div class="container">
        <div class="row">
            <!--start footer-logo-->
            <div class="footer-logo col-xl-4 col-lg-6">
                <img src="/website/images/main/footer-logo.png" alt="logo" />

                <p class="dark-text">
                    @if(app()->isLocale('en'))
                        {{$settings->footer_description_en}}
                    @else
                        {{$settings->footer_description_ar}}

                        @endif
                </p>
            </div>
            <!--end footer-logo-->

            <!--start footer-list-grid-->
            <div class="footer-list-grid col-xl-4 col-lg-6">
                <div class="row no-marg-row">
                    <div class="footer-list col-lg-6 col-sm-6">
                        <a href="/ideas"> <h2 class="footer-title first_color">{{trans('site.wow_ideas')}}</h2></a>
                        <p class="dark-text">
                            @if(app()->isLocale('ar'))
                                مجموعة من الفعاليات والعروض المبتكرة لمختلف المناسبات، اطلع عليها الآن
                            @else
                                A collection of  famous and  innovative Acts that suit different occasions are now available
                            @endif
                        </p>
                        {{--<ul class="list-unstyled">--}}
                            {{--@foreach($ideas as $idea)--}}
                                {{--<li>--}}
                                        {{--@if(app()->isLocale('ar'))--}}
                                            {{--{{$idea->title_ar}}--}}
                                        {{--@else--}}
                                            {{--{{$idea->title_en?:$idea->title_ar}}--}}
                                        {{--@endif--}}
                                {{--</li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}

                    </div>

                    <div class="footer-list col-lg-6 col-sm-6">
                        <h2 class="footer-title first_color">{{trans('site.footer_services')}}</h2>
                        <ul class="list-unstyled">
                            @foreach($services as $service)
                            <li><a href="/services/{{$service->id}}">
                                    @if(app()->isLocale('ar'))
                                        {{$service->title_ar}}
                                    @else
                                        {{$service->title_en?:$service->title_ar}}
                                    @endif</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!--end footer-list-grid-->


            <div class="contact-grid col-xl-4 col-md-12">
                <h2 class="footer-title first_color"> {{trans('site.footer_contacts')}}</h2>
                <ul class="list-unstyled footer-list">
                    <li><a href="tel:{{$settings->phone}}"><i class="fa fa-phone"></i>{{$settings->phone}}</a></li>
                    <li><a href="#"><i class="fa fa-map-marker-alt"></i>{{$settings->address}}</a></li>
                    <li><a href="mailto:{{$settings->email}}"><i class="fa fa-envelope"></i>{{$settings->email}}</a></li>
                </ul>

                <div class="subscribe-form">
                    <form class="needs-validation red-form" novalidate>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="{{trans('site.enter_email')}}" required>
                            <div class="invalid-feedback">
                                please enter valid mail
                            </div>
                            <button type="submit" class="pos-search-btn">{{trans('site.subscribe')}}</button>
                        </div>
                    </form>
                </div>
                <!--start social-grid-->
                <div class="social-div">
                    <ul class="list-unstyled footer-social auto-icon">
                        @if($settings->facebook)
                        <li>
                            <a href="{{$settings->facebook}}" target="_blank" class="fc-icon"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        @endif
                        @if($settings->insta)
                        <li>
                            <a href="{{$settings->insta}}" target="_blank" class="inst-icon"><i class="fab fa-instagram"></i></a>
                        </li>
                        @endif
                        @if($settings->snapchat)
                        <li>
                            <a href="{{$settings->snapchat}}" target="_blank" class="snap-icon"><i class="fab fa-snapchat-ghost"></i></a>
                        </li>
                        @endif
                        @if($settings->youtube)
                        <li>
                            <a href="{{$settings->youtube}}" target="_blank" class="yotube-icon"><i class="fab fa-youtube"></i></a>
                        </li>
                        @endif
                        @if($settings->twitter)
                        <li>
                            <a href="{{$settings->twitter}}" target="_blank" class="tw-icon"><i class="fab fa-twitter"></i></a>
                        </li>
                        @endif

                    </ul>
                </div>
                <!--end social-grid-->
            </div>



            <div class="copyrights col-12 row no-marg-row">
                <div class="col-md-5  text-left-dir jadara-logo sm-center">
                    <a href="https://jaadara.com/" target="_blank" title="jaadara">Designed and implemented by jaadara</a>
                </div>

                <div class="col-md-7 text-right-dir  sm-center">
                    Copyright &copy; 2019 all rights are reserved for Mounasbat
                </div>

            </div>
        </div>
    </div>
</footer>