<!DOCTYPE html>

<html >

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>monsabat</title>

    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="monsabat website" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#da353b">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#da353b">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#da353b">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="/website/images/main/favicon.ico" />



    <!-- Style sheet
         ================ -->
    {{--@if(App::isLocale('en'))--}}

    <link rel="stylesheet" href="/website/css/bootstrap.min.css" type="text/css" />
    {{--@endif--}}
    <!---for arabic style-->
    @if(App::isLocale('ar'))
    <link rel="stylesheet" href="/website/css/bootstrap-rtl.min.css" type="text/css" />
    @endif


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link rel="stylesheet" href="/website/css/animate.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.css" type="text/css" />
    <link rel="stylesheet" href="https://ZulNs.github.io/w3css/w3.css" type="text/css" />
    <link rel="stylesheet" href="/website/css/owl.carousel.min.css" type="text/css" />
    <link rel="stylesheet" href="/website/css/owl.theme.default.min.css" type="text/css" />
    <link rel="stylesheet" href="/website/css/general.css" type="text/css" />
    <link rel="stylesheet" href="/website/css/header.css" type="text/css" />
    <link rel="stylesheet" href="/website/css/footer.css" type="text/css" />
    <link rel="stylesheet" href="/website/css/keyframes.css" type="text/css" />
    <link rel="stylesheet" href="/website/css/stars.css" type="text/css" />
    <!---for arabic style-->
    @if(App::isLocale('ar'))
    <link rel="stylesheet" href="/website/css/ar.css" type="text/css" />
    @endif
    <!---for english style-->
    @if(App::isLocale('en'))
    <link rel="stylesheet" href="/website/css/en.css" type="text/css" />
    @endif
    <!--end-->
    <link rel="stylesheet" href="/website/css/style.css" type="text/css" />
    <link rel="stylesheet" href="/website/css/responsive.css" type="text/css" />
</head>

<body>
<!-- start header
     ================ -->
@include('website.layouts.header')

<!--end header-->




<!-- start navbar
     ================ -->
@include('website.layouts.navbar')

<!--end navbar-->


@yield('content')



<!-- start footer
     ================ -->
@include('website.layouts.footer')

<!--end footer-->




<!-- scripts
     ================ -->
<script type="text/javascript" src="/website/js/html5shiv.min.js"></script>
<script type="text/javascript" src="/website/js/respond.min.js"></script>
<script type="text/javascript" src="/website/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="/website/js/popper.min.js"></script>
<script type="text/javascript" src="/website/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/website/js/html5lightbox.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="/website/js/custom-sweetalert.js"></script>
<script type="text/javascript" src="/website/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/website/js/datepicker_data2.js"></script>
<script type="text/javascript" src="/website/js/datepicker_data.js"></script>
<script src="/website/js/datepicker.js" type="text/javascript"></script>
<script src="/website/js/wow.min.js" type="text/javascript"></script>
<script src="/website/js/custom.js" type="text/javascript"></script>
<script src="/website/js/countries.js" type="text/javascript"></script>
<script src="/website/js/capctcha.js" type="text/javascript"></script>


<script>
    $(".main-carousel").owlCarousel({
        loop: true,
        rtl: true,
        nav: false,
        dots: true,
        items: 1,
        autoplay: true,
        mouseDrag: true,
        touchDrag: true,
        autoplayTimeout: 4000,
    });
</script>
<script>
    $("#input-file").on("change", function () {

    var names = [];
    for (var i = 0; i < $(this).get(0).files.length; ++i) {
        names.push($(this).get(0).files[i].name);
    }
        $(this).siblings(".custom-input-label").find("b").html(names.join(','));
});


</script>
</body>

</html>
