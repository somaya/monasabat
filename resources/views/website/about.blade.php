@extends("website.layouts.app")
@section('content')
    @include('website.layouts.slider')
    <section class="about  gray-bg">
        <div class="container">
            <div class="row">

                <!--start about-img-->
                <div class="about-img-grid col-xl-4 col-md-4 d-md-block d-none">
                    <div class="about-img full-width-img">
                        <div class="starbox about-stars">
                            <div class='dot'></div>
                            <div class='dot1'></div>
                            <div class='dot2'></div>
                        </div>
                        <img src="{{$settings->expressive_image}}" class="converted-img" alt="logo" />
                    </div>
                </div>
                <!--end about-img-->


                <!--start about-text-grid-->
                <div class="about-text-grid col-xl-8 col-md-8">
                    <div class="about-text">
                        @if(app()->isLocale('ar'))
                        {!! $settings->aboutus_ar !!}
                            @else
                            {!! $settings->aboutus_en  !!}
                        @endif
                    </div>
                </div>
                <!--end about-text-grid-->

            </div>
        </div>
    </section>

    @endsection