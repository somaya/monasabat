@extends("website.layouts.app")
@section('content')
    @include('admin.message')

    @include('website.layouts.slider')



    <section class="about  gray-bg">
        <div class="container">
            <div class="row">

                <!--start about-img-->
                <div class="about-img-grid col-xl-4 col-md-4 d-md-block d-none">
                    <div class="about-img full-width-img">
                        <div class="starbox about-stars">
                            <div class='dot'></div>
                            <div class='dot1'></div>
                            <div class='dot2'></div>
                        </div>
                        <img src="{{$settings->expressive_image}}" class="converted-img" alt="logo" />
                    </div>
                </div>
                <!--end about-img-->


                <!--start about-text-grid-->
                <div class="about-text-grid col-xl-8 col-md-8">
                    <div class="about-text">
                        <h2 class="first_color">{{app()->isLocale('ar') ? 'ماذا تعرف عنا ؟':'What do you know about us ?'}}</h2>
                        <p> <b class="first_color">{{app()->isLocale('ar') ? 'مناسبات':'Mounasbat'}}</b>
                            @if(app()->isLocale('en'))
                            is a booking agency for talents and entertainment shows both locally & internationally based in Riyadh City, which became a major attraction and international center for hosting a wide range of largest entertainment, conferences, exhibitions, and events.
                                @else
                                هي وكالة حجوزات وتسويق للمواهب والعروض الترفيهية المحلية والعالمية، تمارس عملها من مقرها الرئيسي في المملكة العربية السعودية وتحديدا مدينة الرياض، العاصمة التي تحولت الى نقطة جذب ومركز عالمي رئيسي لاستضافة أشهر وأكبر الفعاليات الترفيهية والثقافية والرياضية
                            @endif
                        </p>
                    </div>
                    <a href="/aboutus" class="custom-btn">{{trans('site.read_our_story')}}</a>

                </div>
                <!--end about-text-grid-->

            </div>
        </div>
    </section>
    <!--end about-->



    <!-- start news
         ================ -->
    <section class="news margin-index-div  gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="/news" class="sec-title first_color wow fadeInUp" data-hover="{{trans('site.read_more')}}">{{trans('site.entertainment_news')}}<span>{{trans('site.entertainment_news')}}</span></a>
                    <p class="sec-descripe wow fadeInUp">
                        @if(app()->isLocale('en'))
                            {{$settings->news_description_en}}
                        @else
                            {{$settings->news_description_ar}}



                            @endif
                    </p>
                </div>
                @foreach($news as $new)
                <!--start news-grid-->
                <div class="news-grid col-lg-4 col-sm-6 wow fadeIn">
                    <a href="/news/{{$new->id}}">
                        <div class="news-img full-width-img  has_seudo">
                            <img src="{{$new->image}}" class="converted-img" alt="logo" />
                            <div class="news-caption">
                                <h3 class="white-text">
                                    @if(app()->isLocale('ar'))
                                        {{$new->title_ar}}
                                    @else
                                        {{$new->title_en}}
                                    @endif
                                </h3>
                                <p class="white-text">
                                    @if(app()->isLocale('ar'))

                                        {{ str_limit($new->content_ar, $limit = 150, $end = '...') }}
                                    @else
                                        {{ str_limit($new->content_en, $limit = 150, $end = '...') }}



                                        @endif
                                </p>
                            </div>
                            <div class="text-center btn-div"><span class="custom-btn sm-btn">{{trans('site.read_more')}}</span>
                            </div>

                        </div>
                    </a>
                </div>
                <!--end news-grid-->
                @endforeach



            </div>
        </div>
    </section>
    <!--end news-->

    <!-- start news
         ================ -->
    <section class="news margin-index-div vedios-div gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="/artists" class="sec-title first_color wow fadeInUp" data-hover="{{trans('site.read_more')}}">{{trans('site.latest_artists')}}<span>{{trans('site.latest_artists')}}</span></a>
                    <p class="sec-descripe wow fadeInUp">
                        @if(app()->isLocale('en'))
                            {{$settings->new_artists_description_en}}

                        @else
                            {{$settings->new_artists_description_ar}}

                        @endif
                    </p>
                </div>


                <!--start news-grid-->
                @foreach($artists as $artist)

                    <div class="news-grid col-lg-4 col-sm-6 wow fadeIn">
                        <a href="/artists/{{$artist->id}}">
                            <div class="news-img full-width-img  has_seudo">
                                <img src="{{$artist->main_image}}" class="converted-img" alt="logo" />
                                <div class="news-caption">
                                    <h3 class="white-text">{{$artist->name}}</h3>
                                    <p class="white-text">{!! str_limit($artist->description, $limit = 150) !!}</p>
                                </div>
                                <div class="text-center btn-div"><span class="custom-btn sm-btn">{{trans('site.read_more')}}</span>
                                </div>

                            </div>
                        </a>
                    </div>
                @endforeach
                <!--end news-grid-->



            </div>
        </div>
    </section>
    <!--end news-->


    <!-- start news
         ================ -->
    <section class="news margin-index-div vedios-div gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="/events" class="sec-title first_color wow fadeInUp" data-hover="{{trans('site.read_more')}}">{{trans('site.last_events')}}<span>{{trans('site.last_events')}}</span></a>
                    <p class="sec-descripe wow fadeInUp">
                        @if(app()->isLocale('en'))
                            {{$settings->last_events_description_en}}
                        @else
                            {{$settings->last_events_description_ar}}



                        @endif
                    </p>
                </div>
                @foreach($events as $event)

                    <div class="news-grid col-lg-4 col-sm-6 wow fadeIn">
                        <a href="/events/{{$event->id}}">
                            <div class="news-img full-width-img  has_seudo">
                                <img src="{{$event->main_image}}" class="converted-img" alt="logo" />
                                <div class="news-caption">
                                    <h3 class="white-text">{{app()->isLocale('ar') ? $event->title_ar : $event->title_en}}</h3>
                                    <p class="white-text">
                                        @if(app()->isLocale('ar'))
                                        {!! str_limit($event->content_ar, $limit = 150) !!}
                                            @else
                                            {!! str_limit($event->content_en, $limit = 150) !!}



                                        @endif
                                    </p>
                                </div>
                                <div class="text-center btn-div"><span class="custom-btn sm-btn">{{trans('site.read_more')}}</span>
                                </div>

                            </div>
                        </a>
                    </div>
                @endforeach



            </div>
        </div>
    </section>
    <!--end news-->


    <!-- start news
         ================ -->
    <section class="news margin-index-div vedios-div gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="/shows" class="sec-title first_color wow fadeInUp" data-hover="{{trans('site.read_more')}}">{{trans('site.popular_shows')}}<span>{{trans('site.popular_shows')}}</span></a>
                    <p class="sec-descripe wow fadeInUp">
                        @if(app()->isLocale('en'))
                            {{$settings->popular_shows_description_en}}
                            @else
                            {{$settings->popular_shows_description_ar}}



                    @endif
                    </p>
                </div>

                @foreach($shows as $show)

                    <div class="news-grid col-lg-4 col-sm-6 wow fadeIn">
                        <a href="/shows/{{$show->id}}">
                            <div class="news-img full-width-img  has_seudo">
                                <img src="{{$show->main_image}}" class="converted-img" alt="logo" />
                                <div class="news-caption">
                                    <h3 class="white-text">{{app()->isLocale('ar') ? $show->title_ar : $show->title_en }}</h3>
                                    <p class="white-text">
                                        @if(app()->isLocale('ar'))
                                            {!! str_limit($show->content_ar, $limit = 150) !!}
                                        @else
                                            {!! str_limit($show->content_en, $limit = 150) !!}

                                        @endif
                                    </p>
                                </div>
                                <div class="text-center btn-div"><span class="custom-btn sm-btn">{{trans('site.read_more')}}</span>
                                </div>

                            </div>
                        </a>
                    </div>
                @endforeach


            </div>
        </div>
    </section>
    <!--end news-->

@endsection