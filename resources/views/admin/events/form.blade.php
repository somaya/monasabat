<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">عنوان الفاعليه باللغة العربية: </label>
    <div class="col-lg-4{{ $errors->has('title_ar') ? ' has-danger' : '' }}">
        {!! Form::text('title_ar',old('title_ar'),['class'=>'form-control m-input','required','autofocus','placeholder'=> 'عنوان الفاعلية باللغة العربية' ]) !!}
        @if ($errors->has('title_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">عنوان الفاعلية باللغة الانجليزية : </label>
    <div class="col-lg-4{{ $errors->has('title_en') ? ' has-danger' : '' }}">
        {!! Form::text('title_en',old('title_en'),['class'=>'form-control m-input','placeholder'=>'عنوان الفاعلية باللغة الانجليزية']) !!}
        @if ($errors->has('title_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف الفاعلية باللغة العربية: </label>
    <div class="col-lg-10{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
        <textarea name="content_ar" class="form-control summernote " required>{{isset($event) ? $event->content_ar :old('content_ar')}}</textarea>

        @if ($errors->has('content_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('content_ar') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف الفاعلية باللغة الانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
        <textarea name="content_en" class="form-control summernote " >{{isset($event) ? $event->content_en :old('content_en')}}</textarea>

        @if ($errors->has('content_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('content_en') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">تاريخ الفاعلية: </label>
    <div class="col-lg-10{{ $errors->has('date') ? ' has-danger' : '' }}">

        <input type="text" name="date" value="{{isset($event) ?$event->date: old('date') }}" class="form-control ">
        @if ($errors->has('date'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('date') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">الصورة الرئيسية: </label>
    <div class="col-lg-10{{ $errors->has('main_image') ? ' has-danger' : '' }}">

        <input type="file" name="main_image"  class="form-control uploadinput">
        @if ($errors->has('main_image'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('main_image') }}</strong>
            </span>
        @endif
    </div>

</div>

@if(isset($event) )
    <div class="col-12 text-center">
        <img src="{{$event->main_image}}" style="max-width:120px;max-height:120px align="center">

    </div>
@endif

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">صور او فيديوهات أخرى: </label>
    <div class="col-lg-10{{ $errors->has('medias') ? ' has-danger' : '' }}">

        <input type="file" name="medias[]" multiple  class="form-control uploadinput">
        @if ($errors->has('medias'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('medias') }}</strong>
            </span>
        @endif
    </div>

</div>
{{--&& in_array($clinicsmodel->file_type,['jpeg','png','jpg','gif']--}}


@if(isset($event) && count($event->images)>0)
<div class='col-sm-8' style="text-align: center">
    @foreach($event->images as $item)
        @if(in_array($item->type,['jpeg','png','jpg','gif']))
            <img src="{{$item->media}}" style="max-width:120px;max-height:120px " align="center">

        @else
    <video width="120" height="120" controls>
        <source src="{{ $item->media }}" type="video/mp4" style="max-width:120px;max-height:120px ">
        Your browser does not support the video tag.
    </video>
        @endif
        @endforeach
</div>
@endif


