@extends('admin.layouts.app')

@section('title')
    عرض تفاصيل الحجز
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسة</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/artist/accepted')}}" class="m-menu__link">
            <span class="m-menu__link-text">الحجوزات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">عرض تفاصيل الحجز</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        عرض تفاصيل الحجز
                    </h3>
                </div>
            </div>
            {{--<div style="margin-top:12px"><a href="/downloadFile/{{$patient->id}}" style="margin-bottom:20px" class="btn btn_primary btn btn-danger" ><i class=" fa fa-download"></i>{{trans('side.print_patient')}}</a></div>--}}
            {{--<div style="margin-top:12px"> <a href="#" title="{{trans('side.print')}}" class="greyscreen-print" onclick="window.print()"><i class="la la-print"></i></a>--}}
            </div>


        <div class="m-portlet__body">

        <!--begin::Form-->
        {!! Form::model($reservation,['route' => ['reservations.show' , $reservation->id],'method'=> 'GET','class'=>'m-form m-form--fit m-form--label-align-right']) !!}
    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">الاسم الاول : </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="name" value="{{$reservation->f_name}}" class="form-control" disabled>

        </div>

        <label class="col-lg-2 col-form-label">الاسم التانى: </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="email" value="{{$reservation->s_name}}" class="form-control" disabled>

        </div>

    </div>



    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">رقم الهاتف : </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="country" value="{{$reservation->phone}}" class="form-control" disabled>

        </div>

        <label class="col-lg-2 col-form-label">البريد الالكترونى: </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$reservation->email}}" class="form-control" disabled>

        </div>

    </div>
    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">الميزانيه : </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="country" value="{{$reservation->budget}}" class="form-control" disabled>

        </div>

        <label class="col-lg-2 col-form-label">العملة:</label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$reservation->currency->name_ar}}" class="form-control" disabled>

        </div>

    </div>
    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">المدينه : </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="country" value="{{$reservation->city->name_ar}}" class="form-control" disabled>

        </div>

        <label class="col-lg-2 col-form-label">العنوان التفصيلى:</label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$reservation->address}}" class="form-control" disabled>

        </div>

    </div>
        @if($reservation->artist_id)
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">الفنان: </label>
        <div class="col-lg-8{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$reservation->artist->name}}" class="form-control" disabled>
            <a class="col-lg-2" href="/webadmin/artist/accepted">تفاصيل الفنان</a>


        </div>

    </div>
        @endif

    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">الخدمه المطلوبه: </label>
        <div class="col-lg-10{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$reservation->service->title_ar}}" class="form-control" disabled>


        </div>

    </div>

    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">اسم الفاعلية: </label>
        <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$reservation->event_name}}" class="form-control" disabled>

        </div>

    </div>

    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">تاريخ الفاعلية: </label>
        <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$reservation->date}}" class="form-control" disabled>

        </div>

    </div>
        @if($reservation->comments)
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">تفاصيل أخرى: </label>
        <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
            <textarea type="text" name="city"  class="form-control" disabled>{{$reservation->comments}}</textarea>

        </div>

    </div>
            @endif

    </div>
    </div>


@endsection

