<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">احصائيات عامة</span>
            </span>
        </span>
    </a>
</li>
{{--@can('viewAny',\App\patientPublicModels\Patient::class)--}}
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/users" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المستخدمين</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/events" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الفاعليات الجديده</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/shows" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاكثر طلبا </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
    <a href="javascript:;" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon flaticon-customer"></i>
        <span class="m-menu__link-text">الفنانين</span>
        <i class="m-menu__ver-arrow la la-angle-right"></i>
    </a>
    <div class="m-menu__submenu ">
        <span class="m-menu__arrow"></span>
        <ul class="m-menu__subnav">
            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/artist/pending')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">فنانين فى انتظار القبول</span>
                </a>
            </li>
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{url('/webadmin/artist/accepted')}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">فنانين مقبولين</span>
                    </a>
                </li>
            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/artists/create')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">إضافة فنان</span>
                </a>
            </li>
            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/artist/sendEmail')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text"> إرسال ايميل لجميع الفنانين</span>
                </a>
            </li>
        </ul>
    </div>
</li>
<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
    <a href="javascript:;" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon flaticon-customer"></i>
        <span class="m-menu__link-text">أنواع الترفيه</span>
        <i class="m-menu__ver-arrow la la-angle-right"></i>
    </a>
    <div class="m-menu__submenu ">
        <span class="m-menu__arrow"></span>
        <ul class="m-menu__subnav">
            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/entertainments-types')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">أنواع الترفيه الرئيسية</span>
                </a>
            </li>
            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/entertainments-subTypes')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">أنواع الترفيه الفرعية</span>
                </a>
            </li>
        </ul>
    </div>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/reservations" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الحجوزات </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/ideas" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">العروض المميزة </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/services" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الخدمات </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/news" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاخبار </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/countries" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الدول </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/cities" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المدن </span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/currencies" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">العملات </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/eventTypes" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">تصنيفات العروض </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/teams" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text"> فريق العمل </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/contacts" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">  رسائل اتصل بنا </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/sliders" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">  الصور المتحركه </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/settings" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">  الاعدادات </span>
            </span>
        </span>
    </a>
</li>
{{--@endcan--}}


{{--@can('viewAny',\App\Models\Doctors::class)--}}
{{--<li class="m-menu__item" aria-haspopup="true">--}}
    {{--<a href="{{route('doctors.index')}}" class="m-menu__link ">--}}
        {{--<i class="m-menu__link-icon flaticon-line-graph"></i>--}}
        {{--<span class="m-menu__link-title">--}}
            {{--<span class="m-menu__link-wrap">--}}
                {{--<span class="m-menu__link-text">{{ trans('local.doctors') }}</span>--}}
            {{--</span>--}}
        {{--</span>--}}
    {{--</a>--}}
{{--</li>--}}
{{--@endcan--}}
{{--@can('viewAny',\App\patientPublicModels\Modal::class)--}}
{{--<li class="m-menu__item" aria-haspopup="true">--}}
    {{--<a href="{{url('publicmodels')}}" class="m-menu__link ">--}}
        {{--<i class="m-menu__link-icon flaticon-file"></i>--}}
        {{--<span class="m-menu__link-title">--}}
            {{--<span class="m-menu__link-wrap">--}}
                {{--<span class="m-menu__link-text">{{ __('side.public_models') }}</span>--}}
            {{--</span>--}}
        {{--</span>--}}
    {{--</a>--}}
{{--</li>--}}
{{--@endcan--}}
{{--@can('viewAny',\App\patientPublicModels\Modal::class)--}}
{{--<li class="m-menu__item" aria-haspopup="true">--}}
    {{--<a href="{{url('clinicsmodels')}}" class="m-menu__link ">--}}
        {{--<i class="m-menu__link-icon flaticon-file"></i>--}}
        {{--<span class="m-menu__link-title">--}}
            {{--<span class="m-menu__link-wrap">--}}
                {{--<span class="m-menu__link-text">{{ __('side.clinics_models') }}</span>--}}
            {{--</span>--}}
        {{--</span>--}}
    {{--</a>--}}
{{--</li>--}}
{{--@endcan--}}
{{--@can('viewAny',\App\patientPublicModels\Modal::class)--}}
{{--<li class="m-menu__item" aria-haspopup="true">--}}
    {{--<a href="{{url('labsmodels')}}" class="m-menu__link ">--}}
        {{--<i class="m-menu__link-icon flaticon-file"></i>--}}
        {{--<span class="m-menu__link-title">--}}
            {{--<span class="m-menu__link-wrap">--}}
                {{--<span class="m-menu__link-text">{{ __('side.labs_models') }}</span>--}}
            {{--</span>--}}
        {{--</span>--}}
    {{--</a>--}}
{{--</li>--}}
{{--@endcan--}}
{{--@can('viewAny',\App\patientPublicModels\Modal::class)--}}
{{--<li class="m-menu__item" aria-haspopup="true">--}}
    {{--<a href="{{url('operationsmodels')}}" class="m-menu__link ">--}}
        {{--<i class="m-menu__link-icon flaticon-file"></i>--}}
        {{--<span class="m-menu__link-title">--}}
            {{--<span class="m-menu__link-wrap">--}}
                {{--<span class="m-menu__link-text">{{ __('side.operations_models') }}</span>--}}
            {{--</span>--}}
        {{--</span>--}}
    {{--</a>--}}
{{--</li>--}}
{{--@endcan--}}
{{--@can('viewAny',\Spatie\Permission\Models\Permission::class)--}}
{{--<li class="m-menu__item" aria-haspopup="true">--}}
    {{--<a href="{{url('permissions')}}" class="m-menu__link ">--}}
        {{--<i class="m-menu__link-icon flaticon-file"></i>--}}
        {{--<span class="m-menu__link-title">--}}
            {{--<span class="m-menu__link-wrap">--}}
                {{--<span class="m-menu__link-text">{{ __('side.permissions') }}</span>--}}
            {{--</span>--}}
        {{--</span>--}}
    {{--</a>--}}
{{--</li>--}}
{{--@endcan--}}
{{--@can('viewAny',\Spatie\Permission\Models\Role::class)--}}
{{--<li class="m-menu__item" aria-haspopup="true">--}}
    {{--<a href="{{url('roles')}}" class="m-menu__link ">--}}
        {{--<i class="m-menu__link-icon flaticon-file"></i>--}}
        {{--<span class="m-menu__link-title">--}}
            {{--<span class="m-menu__link-wrap">--}}
                {{--<span class="m-menu__link-text">{{ __('side.roles') }}</span>--}}
            {{--</span>--}}
        {{--</span>--}}
    {{--</a>--}}
{{--</li>--}}
{{--@endcan--}}
{{--@can('role-list')--}}
    {{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">--}}
        {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
            {{--<i class="m-menu__link-icon flaticon-customer"></i>--}}
            {{--<span class="m-menu__link-text">نظام الصلاحيات</span>--}}
            {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
        {{--</a>--}}
        {{--<div class="m-menu__submenu ">--}}
            {{--<span class="m-menu__arrow"></span>--}}
            {{--<ul class="m-menu__subnav">--}}
                {{--<li class="m-menu__item" aria-haspopup="true">--}}
                    {{--<a href="{{url('admincp/roles')}}" class="m-menu__link ">--}}
                        {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                        {{--<span class="m-menu__link-text">عرض المجموعات</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--@can('role-create')--}}
                    {{--<li class="m-menu__item" aria-haspopup="true">--}}
                        {{--<a href="{{url('admincp/roles/create')}}" class="m-menu__link ">--}}
                            {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                            {{--<span class="m-menu__link-text">إضافه مجموعه جديده</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--@endcan--}}
            {{--</ul>--}}
        {{--</div>--}}
    {{--</li>--}}
{{--@endcan--}}

{{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">--}}
    {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
        {{--<i class="m-menu__link-icon flaticon-layers"></i>--}}
        {{--<span class="m-menu__link-text">مثال لقائمة</span>--}}
        {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
    {{--</a>--}}
    {{--<div class="m-menu__submenu ">--}}
        {{--<span class="m-menu__arrow"></span>--}}
        {{--<ul class="m-menu__subnav">--}}
            {{--<li class="m-menu__item " aria-haspopup="true">--}}
                {{--<a href="#" class="m-menu__link ">--}}
                    {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot">--}}
                        {{--<span></span>--}}
                    {{--</i>--}}
                    {{--<span class="m-menu__link-text">قائمة أولى</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">--}}
                {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
                    {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                    {{--<span class="m-menu__link-text">قائمة فرعيه</span>--}}
                    {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
                {{--</a>--}}
                {{--<div class="m-menu__submenu ">--}}
                    {{--<span class="m-menu__arrow"></span>--}}
                    {{--<ul class="m-menu__subnav">--}}
                        {{--<li class="m-menu__item " aria-haspopup="true">--}}
                            {{--<a href="#" class="m-menu__link ">--}}
                                {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot">--}}
                                    {{--<span></span>--}}
                                {{--</i>--}}
                                {{--<span class="m-menu__link-text">قائمة</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="m-menu__item " aria-haspopup="true">--}}
                            {{--<a href="#" class="m-menu__link ">--}}
                                {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>--}}
                                {{--<span class="m-menu__link-text">قائمة</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</div>--}}
{{--</li>--}}
{{--<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">--}}
    {{--<a href="javascript:;" class="m-menu__link m-menu__toggle">--}}
        {{--<i class="m-menu__link-icon flaticon-layers"></i>--}}
        {{--<span class="m-menu__link-text">المناطق</span>--}}
        {{--<i class="m-menu__ver-arrow la la-angle-right"></i>--}}
    {{--</a>--}}
    {{--<div class="m-menu__submenu ">--}}
        {{--<span class="m-menu__arrow"></span>--}}
        {{--<ul class="m-menu__subnav">--}}
            {{--<li class="m-menu__item " aria-haspopup="true">--}}
                {{--<a href="/admincp/currencies" class="m-menu__link ">--}}
                    {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot">--}}
                        {{--<span></span>--}}
                    {{--</i>--}}
                    {{--<span class="m-menu__link-text">الدول</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="m-menu__item " aria-haspopup="true">--}}
                {{--<a href="/admincp/cities" class="m-menu__link ">--}}
                    {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot">--}}
                        {{--<span></span>--}}
                    {{--</i>--}}
                    {{--<span class="m-menu__link-text">المدن</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="m-menu__item " aria-haspopup="true">--}}
                {{--<a href="/admincp/zones" class="m-menu__link ">--}}
                    {{--<i class="m-menu__link-bullet m-menu__link-bullet--dot">--}}
                        {{--<span></span>--}}
                    {{--</i>--}}
                    {{--<span class="m-menu__link-text">الاحياء</span>--}}
                {{--</a>--}}
            {{--</li>--}}

        {{--</ul>--}}
    {{--</div>--}}
{{--</li>--}}
