<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">اسم التصنيف بالعربيه: </label>
    <div class="col-lg-4{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم التصنيف بالعربيه"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">اسم التصنيف بالانجليزيه : </label>
    <div class="col-lg-5{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','placeholder'=>"اسم التصنيف بالانجليزيه"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>
</div>



