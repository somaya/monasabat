
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الاسم: </label>
    <div class="col-lg-10{{ $errors->has('name') ? ' has-danger' : '' }}">
        {!! Form::text('name',old('name'),['class'=>'form-control m-input','required','autofocus','placeholder'=> 'الاسم' ]) !!}
        @if ($errors->has('title_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">البريد الالكترونى: </label>
    <div class="col-lg-10{{ $errors->has('email') ? ' has-danger' : '' }}">
        {!! Form::text('email',old('email'),['class'=>'form-control m-input','required','placeholder'=> 'البريد الالكترونى' ]) !!}
        @if ($errors->has('email'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">كلمة المرور: </label>
    <div class="col-lg-10{{ $errors->has('password') ? ' has-danger' : '' }}">
        {!! Form::password('password',['class'=>'form-control m-input','placeholder'=> 'كلمة المرور' ]) !!}
        @if ($errors->has('password'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

</div>


