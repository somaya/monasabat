


<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">الصورة : </label>
    <div class="col-lg-10{{ $errors->has('image') ? ' has-danger' : '' }}">

        <input type="file" name="image"  class="form-control uploadinput">
        @if ($errors->has('image'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
    </div>

</div>

@if(isset($slider) )
    <div class="col-12 text-center">
        <img src="{{$slider->image}}" style="max-width:120px;max-height:120px align="center">

    </div>
@endif


