<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">نوع الترفيه الفرعى بالعربيه: </label>
    <div class="col-lg-4{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>" نوع الترفيه الفرعى بالعربيه"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label"> نوع الترفيه الفرعى بالانجليزيه : </label>
    <div class="col-lg-4{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','placeholder'=>" نوع الترفيه الفرعى بالانجليزيه"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
<label class="col-lg-2 col-form-label">الترفيه الرئيسى : </label>
<div class="col-lg-10{{ $errors->has('category_id') ? ' has-danger' : '' }}">
    <select name="category_id" class="form-control">
        <option value="" disabled selected>اختر  الترفيه الرئيسى</option>
        @foreach($types as $type)
        <option value="{{$type->id}}" {{isset($subtype) && $subtype->category_id==$type->id?'selected':'' }} {{old('category_id')==$type->id? 'selected':''}}>{{$type->name_ar}}</option>
            @endforeach

    </select>
    @if ($errors->has('category_id'))
        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
    @endif
</div>
</div>
