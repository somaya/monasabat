@extends('admin.layouts.app')
@section('title')
    إحصائيات عامة
@endsection

@section('header')
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">

            <!-- Traffic sources -->
            <div class="panel panel-flat home-custom-styles">
                <div class="panel-heading">
                    <!-- <h6 class="panel-title">Traffic sources</h6> -->

                </div>

                <div class="container-fluid">
                    <div class="row">

                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-user-check"></i>
                                    </li>
                                    <li class="text-center">
                                        <div class="text-semibold">الفنانين فى انتظار القبول  </div>
                                        <div class="text-muted">{{$pending_artists}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-users"></i>
                                    </li>
                                    <li class="text-center">
                                        <div class="text-semibold">الفنانين المقبولين</div>
                                        <div class="text-muted"><span
                                                    class="status-mark border-success position-left"></span>
                                            {{$accepted_artists}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-droplet2"></i>
                                    </li>
                                    <li class="text-center">
                                        <div class="text-semibold">الفاعليات</div>
                                        <div class="text-muted">{{$events}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-droplet2"></i>
                                    </li>
                                    <li class="text-center">
                                        <div class="text-semibold">العروض</div>
                                        <div class="text-muted">{{$shows}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-droplet2"></i>
                                    </li>
                                    <li class="text-center">
                                        <div class="text-semibold">العروض المميزه</div>
                                        <div class="text-muted">{{$ideas}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-droplet2"></i>
                                    </li>
                                    <li class="text-center">
                                        <div class="text-semibold">الحجوزات</div>
                                        <div class="text-muted">{{$reservations}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>



                    </div>
                </div>


            </div>
            <!-- /traffic sources -->

        </div>

        <div class="col-lg-5">


        </div>
    </div>
@endsection
@section('footer')
@endsection
