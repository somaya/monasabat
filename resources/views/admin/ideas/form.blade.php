
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">عنوان العرض باللغة العربية: </label>
    <div class="col-lg-4{{ $errors->has('title_ar') ? ' has-danger' : '' }}">
        {!! Form::text('title_ar',old('title_ar'),['class'=>'form-control m-input','required','autofocus','placeholder'=> 'عنوان الفاعلية باللغة العربية' ]) !!}
        @if ($errors->has('title_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">عنوان العرض باللغة الانجليزية : </label>
    <div class="col-lg-4{{ $errors->has('title_en') ? ' has-danger' : '' }}">
        {!! Form::text('title_en',old('title_en'),['class'=>'form-control m-input','placeholder'=>'عنوان الفاعلية باللغة الانجليزية']) !!}
        @if ($errors->has('title_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف العرض باللغة العربية: </label>
    <div class="col-lg-10{{ $errors->has('description_ar') ? ' has-danger' : '' }}">
        <textarea name="description_ar" class="form-control summernote " required>{{isset($idea) ? $idea->description_ar :old('description_ar')}}</textarea>

        @if ($errors->has('description_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('description_ar') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف العرض باللغة الانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('description_en') ? ' has-danger' : '' }}">
        <textarea name="description_en" class="form-control summernote " >{{isset($idea) ? $idea->description_en :old('description_en')}}</textarea>

        @if ($errors->has('description_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('description_en') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">الصورة : </label>
    <div class="col-lg-10{{ $errors->has('image') ? ' has-danger' : '' }}">

        <input type="file" name="image"  class="form-control uploadinput">
        @if ($errors->has('image'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
    </div>

</div>

@if(isset($idea) )
    <div class="col-12 text-center">
        <img src="{{$idea->image}}" style="max-width:120px;max-height:120px align="center">

    </div>
@endif


