@extends('admin.layouts.app')

@section('title')
    عرض تفاصيل عضو بالفريق
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسة</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/teams')}}" class="m-menu__link">
            <span class="m-menu__link-text">فريق العمل </span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">عرض تفاصيل عضو بالفريق </span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                         عرض تفاصيل عضو بالفريق
                    </h3>
                </div>
            </div>
            {{--<div style="margin-top:12px"><a href="/downloadFile/{{$patient->id}}" style="margin-bottom:20px" class="btn btn_primary btn btn-danger" ><i class=" fa fa-download"></i>{{trans('side.print_patient')}}</a></div>--}}
            {{--<div style="margin-top:12px"> <a href="#" title="{{trans('side.print')}}" class="greyscreen-print" onclick="window.print()"><i class="la la-print"></i></a>--}}
            </div>


        <div class="m-portlet__body">

        <!--begin::Form-->
        {!! Form::model($team,['route' => ['teams.show' , $team->id],'method'=> 'GET','class'=>'m-form m-form--fit m-form--label-align-right']) !!}
        {{--start patient data--}}

        <div class="form-group m-form__group row">
            <label class="col-lg-2 col-form-label">اسم العضو: </label>
            <div class="col-lg-4{{ $errors->has('name') ? ' has-danger' : '' }}">
                {!! Form::text('name',old('name'),['class'=>'form-control m-input','required','autofocus','placeholder'=> 'اسم العضو' ]) !!}
                @if ($errors->has('name'))
                    <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                @endif
            </div>
            <label class="col-lg-2 col-form-label">الوظيفه : </label>
            <div class="col-lg-4{{ $errors->has('job') ? ' has-danger' : '' }}">
                {!! Form::text('job',old('job'),['class'=>'form-control m-input','required','placeholder'=>'الوظيفة']) !!}
                @if ($errors->has('job'))
                    <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('job') }}</strong>
            </span>
                @endif
            </div>
        </div>

        <div class="form-group m-form__group row">
            <label class="col-lg-2 col-form-label">حساب الفيسبوك: </label>
            <div class="col-lg-4{{ $errors->has('facebook') ? ' has-danger' : '' }}">
                {!! Form::text('facebook',old('facebook'),['class'=>'form-control m-input','required','autofocus','placeholder'=> 'حساب الفيسبوك' ]) !!}
                @if ($errors->has('facebook'))
                    <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('facebook') }}</strong>
            </span>
                @endif
            </div>
            <label class="col-lg-2 col-form-label">حساب تويتر : </label>
            <div class="col-lg-4{{ $errors->has('twitter') ? ' has-danger' : '' }}">
                {!! Form::text('twitter',old('twitter'),['class'=>'form-control m-input','required','placeholder'=>'حساب تويتر']) !!}
                @if ($errors->has('twitter'))
                    <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('twitter') }}</strong>
            </span>
                @endif
            </div>
        </div>

        <div class="form-group m-form__group row">
            <label class="col-lg-2 col-form-label">حساب اللينكد ان : </label>
            <div class="col-lg-4{{ $errors->has('linkedin') ? ' has-danger' : '' }}">
                {!! Form::text('linkedin',old('linkedin'),['class'=>'form-control m-input','required','autofocus','placeholder'=> 'حساب اللينكد ان' ]) !!}
                @if ($errors->has('linkedin'))
                    <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('linkedin') }}</strong>
            </span>
                @endif
            </div>
            <label class="col-lg-2 col-form-label">حساب الانستجرام : </label>
            <div class="col-lg-4{{ $errors->has('insta') ? ' has-danger' : '' }}">
                {!! Form::text('insta',old('insta'),['class'=>'form-control m-input','required','placeholder'=>'حساب الانستجرام']) !!}
                @if ($errors->has('insta'))
                    <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('insta') }}</strong>
            </span>
                @endif
            </div>
        </div>

    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">الصوره : </label>
        <div class="col-lg-10{{ $errors->has('date') ? ' has-danger' : '' }}">

            <img src="{{$team->photo}}" style="max-width:120px;max-height:120px align="center">

        </div>

    </div>
    </div>
    </div>



@endsection

