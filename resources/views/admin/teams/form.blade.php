
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم العضو: </label>
    <div class="col-lg-4{{ $errors->has('name') ? ' has-danger' : '' }}">
        {!! Form::text('name',old('name'),['class'=>'form-control m-input','required','autofocus','placeholder'=> 'اسم العضو' ]) !!}
        @if ($errors->has('name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">الوظيفه : </label>
    <div class="col-lg-4{{ $errors->has('job') ? ' has-danger' : '' }}">
        {!! Form::text('job',old('job'),['class'=>'form-control m-input','required','placeholder'=>'الوظيفة']) !!}
        @if ($errors->has('job'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('job') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">حساب الفيسبوك: </label>
    <div class="col-lg-4{{ $errors->has('facebook') ? ' has-danger' : '' }}">
        {!! Form::text('facebook',old('facebook'),['class'=>'form-control m-input','placeholder'=> 'حساب الفيسبوك' ]) !!}
        @if ($errors->has('facebook'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('facebook') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">حساب تويتر : </label>
    <div class="col-lg-4{{ $errors->has('twitter') ? ' has-danger' : '' }}">
        {!! Form::text('twitter',old('twitter'),['class'=>'form-control m-input','placeholder'=>'حساب تويتر']) !!}
        @if ($errors->has('twitter'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('twitter') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">حساب اللينكد ان : </label>
    <div class="col-lg-4{{ $errors->has('linkedin') ? ' has-danger' : '' }}">
        {!! Form::text('linkedin',old('linkedin'),['class'=>'form-control m-input','placeholder'=> 'حساب اللينكد ان' ]) !!}
        @if ($errors->has('linkedin'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('linkedin') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">حساب الانستجرام : </label>
    <div class="col-lg-4{{ $errors->has('insta') ? ' has-danger' : '' }}">
        {!! Form::text('insta',old('insta'),['class'=>'form-control m-input','placeholder'=>'حساب الانستجرام']) !!}
        @if ($errors->has('insta'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('insta') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">الصورة : </label>
    <div class="col-lg-10{{ $errors->has('photo') ? ' has-danger' : '' }}">

        <input type="file" name="photo"  class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>

@if(isset($team) )
    <div class="col-12 text-center">
        <img src="{{$team->photo}}" style="max-width:120px;max-height:120px align="center">

    </div>
@endif


