<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم المدينه بالعربيه: </label>
    <div class="col-lg-4{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم المدينه بالعربيه"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">اسم المدينه بالانجليزيه : </label>
    <div class="col-lg-4{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','placeholder'=>"اسم المدينه بالانجليزيه"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
<label class="col-lg-2 col-form-label">الدوله : </label>
<div class="col-lg-10{{ $errors->has('country_id') ? ' has-danger' : '' }}">
    <select name="country_id" class="form-control">
        <option value="" disabled selected>اختر  الدوله</option>
        @foreach($countries as $country)
        <option value="{{$country->id}}" {{isset($city) && $city->country_id==$country->id?'selected':'' }} {{old('country_id')==$country->id? 'selected':''}}>{{$country->name_ar}}</option>
            @endforeach

    </select>
    @if ($errors->has('country_id'))
        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('country_id') }}</strong>
            </span>
    @endif
</div>
</div>
