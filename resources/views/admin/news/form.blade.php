
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">عنوان الخبر باللغة العربية: </label>
    <div class="col-lg-4{{ $errors->has('title_ar') ? ' has-danger' : '' }}">
        {!! Form::text('title_ar',old('title_ar'),['class'=>'form-control m-input','autofocus','placeholder'=> 'عنوان الخبر باللغة العربية' ]) !!}
        @if ($errors->has('title_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">عنوان الخبر باللغة الانجليزية : </label>
    <div class="col-lg-4{{ $errors->has('title_en') ? ' has-danger' : '' }}">
        {!! Form::text('title_en',old('title_en'),['class'=>'form-control m-input','placeholder'=>'عنوان الخبر باللغة الانجليزية']) !!}
        @if ($errors->has('title_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">محتوى الخبر باللغة العربية: </label>
    <div class="col-lg-10{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
        <textarea name="content_ar" class="form-control summernote " >{{isset($new) ? $new->content_ar :old('content_ar')}}</textarea>

        @if ($errors->has('content_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('content_ar') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">محتوى الخبر باللغة الانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
        <textarea name="content_en" class="form-control summernote " >{{isset($new) ? $new->content_en :old('content_en')}}</textarea>

        @if ($errors->has('content_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('content_en') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">الصورة : </label>
    <div class="col-lg-10{{ $errors->has('image') ? ' has-danger' : '' }}">

        <input type="file" name="image"  class="form-control uploadinput">
        @if ($errors->has('image'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
    </div>

</div>

@if(isset($new) )
    <div class="col-12 text-center">
        <img src="{{$new->image}}" style="max-width:120px;max-height:120px align="center">

    </div>
@endif


