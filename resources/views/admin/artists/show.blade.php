@extends('admin.layouts.app')

@section('title')
    عرض تفاصيل الفنان
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسة</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/artist/accepted')}}" class="m-menu__link">
            <span class="m-menu__link-text">الفنانين</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">عرض تفاصيل الفنان</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        عرض تفاصيل الفنان
                    </h3>
                </div>
            </div>
            {{--<div style="margin-top:12px"><a href="/downloadFile/{{$patient->id}}" style="margin-bottom:20px" class="btn btn_primary btn btn-danger" ><i class=" fa fa-download"></i>{{trans('side.print_patient')}}</a></div>--}}
            {{--<div style="margin-top:12px"> <a href="#" title="{{trans('side.print')}}" class="greyscreen-print" onclick="window.print()"><i class="la la-print"></i></a>--}}
            </div>

        <div class="m-portlet__body">


        <!--begin::Form-->
        {!! Form::model($artist,['route' => ['artists.show' , $artist->id],'method'=> 'GET','class'=>'m-form m-form--fit m-form--label-align-right']) !!}
    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">اسم الفنان : </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="name" value="{{$artist->name}}" class="form-control" disabled>

        </div>

        <label class="col-lg-2 col-form-label">البريد الالكترونى: </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="email" value="{{$artist->email}}" class="form-control" disabled>

        </div>

    </div>



    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">دولة الاقامه : </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="country" value="{{$artist->city->country->name_ar}}" class="form-control" disabled>

        </div>

        <label class="col-lg-2 col-form-label">مدينة الاقامه: </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$artist->contact_number}}" class="form-control" disabled>

        </div>

    </div>
    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">رقم الهاتف : </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="country" value="{{$artist->city->country->name_ar}}" class="form-control" disabled>

        </div>

        <label class="col-lg-2 col-form-label">للرقم للتواصل السريع: </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$artist->quick_response_number}}" class="form-control" disabled>

        </div>

    </div>
    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">الجنس : </label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="country" value="{{$artist->gender==1 ?'ذكر ' : 'أنثى'}}" class="form-control" disabled>

        </div>

        <label class="col-lg-2 col-form-label">الجنسية:</label>
        <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$artist->nationality}}" class="form-control" disabled>

        </div>

    </div>
        @if($artist->facebook)
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">فيسبوك: </label>
        <div class="col-lg-10{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$artist->facebook}}" class="form-control" disabled>


        </div>

    </div>
        @endif
        @if($artist->twitter)
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">تويتر: </label>
        <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$artist->twitter}}" class="form-control" disabled>

        </div>

    </div>
        @endif
        @if($artist->insta)
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">انستجرام: </label>
        <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$artist->insta}}" class="form-control" disabled>

        </div>

    </div>
        @endif
        @if($artist->snapchat)
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">سناب شات: </label>
        <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$artist->snapchat}}" class="form-control" disabled>

        </div>

    </div>
        @endif
        @if($artist->pintrest)
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">pintrest: </label>
        <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
            <input type="text" name="city" value="{{$artist->pintrest}}" class="form-control" disabled>

        </div>

    </div>
        @endif


    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">الصوره الرئيسيه: </label>
        <div class="col-lg-10{{ $errors->has('date') ? ' has-danger' : '' }}">

            <img src="{{$artist->main_image}}" style="max-width:120px;max-height:120px align="center">

        </div>

    </div>
        <fieldset>
            <legend>تفاصيل العرض </legend>

            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">اسم العرض: </label>
                <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
                    <input type="text" name="city" value="{{$artist->show_name}}" class="form-control" disabled>

                </div>

            </div>
            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">نوع الترفيه: </label>
                <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
                    <input type="text" name="city" value="{{$artist->entertainment->entertainment->name_ar }}-{{$artist->entertainment->name_ar }}" class="form-control" disabled>

                </div>

            </div>
            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">تصنيف العرض: </label>
                <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
                    @foreach($artist->eventType as $item)

                    <input type="text" name="city" value="{{$item->name_ar }}" class="form-control" disabled>
                        <br>
                        @endforeach

                </div>

            </div>
            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">تفاصيل العرض: </label>
                <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
                    <textarea type="text" name="city"  class="form-control" disabled>{{$artist->description}}</textarea>

                </div>

            </div>
            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">معدات مطلوبه: </label>
                <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
                    <textarea type="text" name="city"  class="form-control" disabled>{{$artist->required_equipment}}</textarea>

                </div>

            </div>
            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">المدة الزمنيه والرسوم التقريبيه: </label>
                <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
                    <input type="text" name="city" value="{{$artist->duration }}" class="form-control" disabled>

                </div>

            </div>

        </fieldset>
    @if(isset($artist) && count($artist->images)>0)
        <label class="col-lg-2 col-form-label">صور وفيديوهات أخرى: </label>

<div class='col-12 row show-event-images' style="text-align: center">
            @foreach($artist->images as $item)
                @if(in_array($item->type,['jpeg','png','jpg','gif']))
                              <div class="col-md-3 col-sm-6">
 <img src="{{$item->media}}" align="center">
</div>
                @else
                             <div class="col-md-3 col-sm-6">
  <video width="100%" height="120" controls>
                        <source src="{{ $item->media }}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                    </div>
                @endif
            @endforeach
        </div>
    @endif
    </div>
    </div>


@endsection

