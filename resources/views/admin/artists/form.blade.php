<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم الفنان : </label>
    <div class="col-lg-4{{ $errors->has('name') ? ' has-danger' : '' }}">
        <input type="text" name="name" value="{{isset($artist) ? $artist->name :old('name')}}" class="form-control"required >
        @if ($errors->has('name'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('name') }}</strong>
                            </span>
        @endif

    </div>

    <label class="col-lg-2 col-form-label">البريد الالكترونى: </label>
    <div class="col-lg-4{{ $errors->has('email') ? ' has-danger' : '' }}">
        <input type="text" name="email" value="{{isset($artist)? $artist->email:old('email')}}" class="form-control" required>
        @if ($errors->has('email'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('email') }}</strong>
                            </span>
        @endif

    </div>

</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">دولة الاقامه : </label>
    <div class="col-lg-4{{ $errors->has('country') ? ' has-danger' : '' }}">
        {{--<input type="text" name="country" value="{{$artist->city->country->name_ar}}" class="form-control" disabled>--}}
         <select name="country" class="form-control" id="state" data-lang="{{app()->getLocale()}}" required>
            <option value="" disabled>اختر الدولة</option>
            @foreach($countries as $country)
                <option value="{{$country->id}}" {{isset($artist) && $artist->city->country->id==$country->id ? 'selected':''}} {{old('country')==$country->id ?'selected':''}}>{{$country->name_ar}}</option>
            @endforeach

        </select>
        @if ($errors->has('country'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('country') }}</strong>
                            </span>
        @endif
    </div>

    <label class="col-lg-2 col-form-label">مدينة الاقامه: </label>
    <div class="col-lg-4{{ $errors->has('city_id') ? ' has-danger' : '' }}">
        <select type="text" id="city_id" name="city_id" class="form-control" required>
            @if(isset($artist))
                <option value="{{$artist->city_id}}">{{$artist->city->name_ar}}</option>

            @endif

        </select>
        @if ($errors->has('city_id'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('city_id') }}</strong>
                            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">رقم الهاتف : </label>
    <div class="col-lg-4{{ $errors->has('contact_number') ? ' has-danger' : '' }}">
        <input type="text" name="contact_number" value="{{isset($artist) ? $artist->contact_number :old('contact_number')}} "  class="form-control" >
        @if ($errors->has('contact_number'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('contact_number') }}</strong>
                            </span>
        @endif

    </div>

    <label class="col-lg-2 col-form-label">للرقم للتواصل السريع: </label>
    <div class="col-lg-4{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
        <input type="text" name="quick_response_number" value="{{isset($artist) ? $artist->quick_response_number :old('quick_response_number')}}" class="form-control" >

        @if ($errors->has('quick_response_number'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('quick_response_number') }}</strong>
                            </span>
        @endif

    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الجنس : </label>
    <div class="col-lg-4{{ $errors->has('gender') ? ' has-danger' : '' }}">
        <select name="gender" class="form-control" required>
            <option value="" disabled>اختر الجنس</option>
            <option value="1" {{isset($artist) && $artist->gender==1 ?'selected':''}} {{old('gender')==1 ?'selected':''}}>ذكر</option>
            <option value="2" {{isset($artist) && $artist->gender==2 ?'selected':''}} {{old('gender')==2 ?'selected':''}}>أنثى</option>


        </select>
        @if ($errors->has('gender'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('gender') }}</strong>
                            </span>
        @endif

    </div>

    <label class="col-lg-2 col-form-label">الجنسية:</label>
    <div class="col-lg-4{{ $errors->has('nationality') ? ' has-danger' : '' }}">
        <input type="text" name="nationality" value="{{isset($artist)? $artist->nationality:old('nationality')}}" class="form-control" >
        @if ($errors->has('nationality'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('nationality') }}</strong>
                            </span>
        @endif

    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">فيسبوك: </label>
    <div class="col-lg-10{{ $errors->has('facebook') ? ' has-danger' : '' }}">
        <input type="url" name="facebook" value="{{isset($artist)? $artist->facebook:old('facebook')}}" class="form-control" >
        @if ($errors->has('facebook'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('facebook') }}</strong>
                            </span>
        @endif


    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">تويتر: </label>
    <div class="col-lg-10{{ $errors->has('twitter') ? ' has-danger' : '' }}">
        <input type="url" name="twitter" value="{{isset($artist)? $artist->twitter:old('twitter')}}" class="form-control" >
        @if ($errors->has('twitter'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('twitter') }}</strong>
                            </span>
        @endif

    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">انستجرام: </label>
    <div class="col-lg-10{{ $errors->has('insta') ? ' has-danger' : '' }}">
        <input type="url" name="insta" value="{{isset($artist)? $artist->insts:old('insta')}}" class="form-control" >
        @if ($errors->has('insta'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('insta') }}</strong>
                            </span>
        @endif

    </div>

</div>

<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">سناب شات: </label>
    <div class="col-lg-10{{ $errors->has('snapchat') ? ' has-danger' : '' }}">
        <input type="url" name="snapchat" value="{{isset($artist)? $artist->snapchat:old('snapchat')}}" class="form-control" >
        @if ($errors->has('snapchat'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('snapchat') }}</strong>
                            </span>
        @endif

    </div>

</div>

<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">pintrest: </label>
    <div class="col-lg-10{{ $errors->has('pintrest') ? ' has-danger' : '' }}">
        <input type="url" name="pintrest" value="{{isset($artist)? $artist->pintrest:old('pintrest')}}" class="form-control" >
        @if ($errors->has('pintrest'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('pintrest') }}</strong>
                            </span>
        @endif

    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">اسم العرض: </label>
    <div class="col-lg-10{{ $errors->has('show_name') ? ' has-danger' : '' }}">
        <input type="text" name="show_name" value="{{isset($artist)? $artist->show_name:old('show_name')}}" class="form-control" >
        @if ($errors->has('show_name'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('show_name') }}</strong>
                            </span>
        @endif

    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">نوع الترفيه: </label>
    <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
        <select type="text" name="entertainment_category" class="form-control" id="entertainment_category" required>
            <option value=""  disabled selected>{{app()->isLocale('en')? 'please select':'من فضلك اختر'}}</option>
            @foreach($entertainments as $entertainment)
                <option value="{{$entertainment->id}}" {{isset($artist) && $artist->entertainment->entertainment->id==$entertainment->id ? 'selected':''}} {{old('entertainment_category')==$entertainment->id ?'selected':''}}>{{app()->isLocale('ar') ? $entertainment->name_ar :$entertainment->name_en}}</option>
            @endforeach
        </select>

    </div>

</div>
<div class="form-group m-form__group row">

    {{--<label class="col-lg-2 col-form-label">نوع الترفيه: </label>--}}
    <div class="col-lg-10{{ $errors->has('entertainment_id') ? ' has-danger' : '' }}">
        <select type="text" name="entertainment_id" class="form-control" id="entertainment_subcategory" required>
            @if(isset($artist))
                <option value="{{$artist->entertainment_id}}">{{$artist->entertainment->name_ar}}</option>

            @endif


        </select>
        @if ($errors->has('entertainment_id'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('entertainment_id') }}</strong>
                            </span>
        @endif
    </div>

</div>
{{--{{dd($artist->eventType()->pluck('event_type_id')->ToArray())}}--}}

<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">تصنيف العرض: </label>
    <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
        <select type="text" name="event_type_ids[]" class="form-control" required multiple>
            <option value="" disabled >{{app()->isLocale('en')? 'please select':'من فضلك اختر'}}</option>
            @foreach($event_types as $event_type)
                <option value="{{$event_type->id}}" {{old('event_type_id')==$event_type->id ? 'selected':''}}{{isset($artist) && in_array($event_type->id,$artist->eventType()->pluck('event_type_id')->ToArray()) ?'selected':'' }}>{{app()->isLocale('ar') ? $event_type->name_ar :$event_type->name_en}}</option>
            @endforeach
        </select>
        @if ($errors->has('event_type_ids'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('event_type_ids') }}</strong>
                            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">تفاصيل العرض: </label>
    <div class="col-lg-10{{ $errors->has('description') ? ' has-danger' : '' }}">
        <textarea type="text" name="description" class="form-control summernote" >{{isset($artist) ? $artist->description :old('description')}}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('description') }}</strong>
                            </span>
        @endif

    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">معدات مطلوبه: </label>
    <div class="col-lg-10{{ $errors->has('required_equipment') ? ' has-danger' : '' }}">
        <textarea type="text" name="required_equipment" class="form-control summernote" >{{isset($artist) ? $artist->required_equipment :old('required_equipment')}}</textarea>
        @if ($errors->has('required_equipment'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('required_equipment') }}</strong>
                            </span>
        @endif

    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">المدة الزمنيه والرسوم التقريبيه: </label>
    <div class="col-lg-10{{ $errors->has('duration') ? ' has-danger' : '' }}">
        <input type="text" name="duration" value="{{isset($artist) ? $artist->duration:old('duration') }}" class="form-control" >
        @if ($errors->has('duration'))
            <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('duration') }}</strong>
                            </span>
        @endif

    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">الصورة الرئيسية: </label>
    <div class="col-lg-10{{ $errors->has('main_image') ? ' has-danger' : '' }}">

        <input type="file" name="main_image" class="form-control uploadinput">
        @if ($errors->has('main_image'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('main_image') }}</strong>
            </span>
        @endif
    </div>

</div>


@if(isset($artist) )
    <div class="col-12 text-center">
        <img src="{{$artist->main_image}}" style="max-width:120px;max-height:120px align=" center">

    </div>
@endif

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">صور او فيديوهات أخرى: </label>
    <div class="col-lg-10{{ $errors->has('medias') ? ' has-danger' : '' }}">

        <input type="file" name="medias[]" multiple class="form-control uploadinput">
        @if ($errors->has('medias'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('medias') }}</strong>
            </span>
        @endif
    </div>

</div>
{{--&& in_array($clinicsmodel->file_type,['jpeg','png','jpg','gif']--}}


@if(isset($artist) && count($artist->images)>0)
    <div class='col-sm-8' style="text-align: center">
        @foreach($artist->images as $item)
            @if(in_array($item->type,['jpeg','png','jpg','gif']))
                <img src="{{$item->media}}" style="max-width:120px;max-height:120px " align="center">

            @else
                <video width="120" height="120" controls>
                    <source src="{{ $artist->media }}" type="video/mp4" style="max-width:120px;max-height:120px ">
                    Your browser does not support the video tag.
                </video>
            @endif
        @endforeach
    </div>
@endif


