@extends('admin.layouts.app')
@section('title')
الاعدادات
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item active-top-bar">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text"> الاعدادات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>

@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        الاعدادات
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_country">
                <thead>
                <tr>
                    <th> البريد الالكترونى</th>
                    <th>رقم الهاتف</th>
                    <th>العنوان</th>
                    <th>الأدوات</th>
                </tr>
                </thead>
                <tbody>
                {{--@foreach($cities as $index=>$city)--}}
                    <tr>
                        <td>{{$setting->email}}</td>
                        <td>{{$setting->phone}}</td>
                        <td>{{$setting->address}}</td>
                        <td>
                            <a  title="تعديل" href="/webadmin/settings/{{$setting->id}}/edit" ><i class="fa fa-edit"></i></a>

                        </td>
                    </tr>
                {{--@endforeach--}}
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/currencies.js') !!}--}}
@endsection
