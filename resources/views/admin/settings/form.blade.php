
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">ماذا تعرف عنا بالعربية: </label>
    <div class="col-lg-10{{ $errors->has('aboutus_ar') ? ' has-danger' : '' }}">
        <textarea name="aboutus_ar" class="form-control summernote " required>{{isset($setting) ? $setting->aboutus_ar :old('aboutus_ar')}}</textarea>

        @if ($errors->has('aboutus_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('aboutus_ar') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">ماذا تعرف عنا بالانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('aboutus_en') ? ' has-danger' : '' }}">
        <textarea name="aboutus_en" class="form-control summernote " required>{{isset($setting) ? $setting->aboutus_en :old('aboutus_en')}}</textarea>

        @if ($errors->has('aboutus_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('aboutus_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف تسجيل الفنان بالعربية: </label>
    <div class="col-lg-10{{ $errors->has('artist_registeration_description_ar') ? ' has-danger' : '' }}">
        <textarea name="artist_registeration_description_ar" class="form-control summernote " required>{{isset($setting) ? $setting->artist_registeration_description_ar :old('artist_registeration_description_ar')}}</textarea>

        @if ($errors->has('artist_registeration_description_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('artist_registeration_description_ar') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف تسجيل الفنان بالانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('artist_registeration_description_en') ? ' has-danger' : '' }}">
        <textarea name="artist_registeration_description_en" class="form-control summernote " required>{{isset($setting) ? $setting->artist_registeration_description_en :old('artist_registeration_description_en')}}</textarea>

        @if ($errors->has('artist_registeration_description_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('artist_registeration_description_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف أخبار الترفيه بالعربية: </label>
    <div class="col-lg-10{{ $errors->has('news_description_ar') ? ' has-danger' : '' }}">
        <textarea name="news_description_ar" class="form-control summernote " required>{{isset($setting) ? $setting->news_description_ar :old('news_description_ar')}}</textarea>

        @if ($errors->has('news_description_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('news_description_ar') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف أخبار الترفيه بالانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('news_description_en') ? ' has-danger' : '' }}">
        <textarea name="news_description_en" class="form-control summernote " required>{{isset($setting) ? $setting->news_description_en :old('news_description_en')}}</textarea>

        @if ($errors->has('news_description_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('news_description_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف الفنانين الجدد بالعربية: </label>
    <div class="col-lg-10{{ $errors->has('new_artists_description_ar') ? ' has-danger' : '' }}">
        <textarea name="new_artists_description_ar" class="form-control summernote " required>{{isset($setting) ? $setting->new_artists_description_ar :old('new_artists_description_ar')}}</textarea>

        @if ($errors->has('new_artists_description_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('new_artists_description_ar') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف الفنانين الجدد بالانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('new_artists_description_en') ? ' has-danger' : '' }}">
        <textarea name="new_artists_description_en" class="form-control summernote " required>{{isset($setting) ? $setting->new_artists_description_en :old('new_artists_description_en')}}</textarea>

        @if ($errors->has('new_artists_description_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('new_artists_description_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف اخر الفاعليات بالانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('last_events_description_en') ? ' has-danger' : '' }}">
        <textarea name="last_events_description_en" class="form-control summernote " required>{{isset($setting) ? $setting->last_events_description_en :old('last_events_description_en')}}</textarea>

        @if ($errors->has('last_events_description_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('last_events_description_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف اخر الفاعليات بالعربية: </label>
    <div class="col-lg-10{{ $errors->has('last_events_description_ar') ? ' has-danger' : '' }}">
        <textarea name="last_events_description_ar" class="form-control summernote " required>{{isset($setting) ? $setting->last_events_description_ar :old('last_events_description_ar')}}</textarea>

        @if ($errors->has('last_events_description_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('last_events_description_ar') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف الاكثر طلبا بالعربية: </label>
    <div class="col-lg-10{{ $errors->has('popular_shows_description_ar') ? ' has-danger' : '' }}">
        <textarea name="popular_shows_description_ar" class="form-control summernote " required>{{isset($setting) ? $setting->popular_shows_description_ar :old('popular_shows_description_ar')}}</textarea>

        @if ($errors->has('popular_shows_description_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('popular_shows_description_ar') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف الاكثر طلبا بالانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('popular_shows_description_en') ? ' has-danger' : '' }}">
        <textarea name="popular_shows_description_en" class="form-control summernote " required>{{isset($setting) ? $setting->popular_shows_description_en :old('popular_shows_description_en')}}</textarea>

        @if ($errors->has('popular_shows_description_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('popular_shows_description_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف الموقع بالفوتر بالانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('footer_description_en') ? ' has-danger' : '' }}">
        <textarea name="footer_description_en" class="form-control summernote " required>{{isset($setting) ? $setting->footer_description_en :old('footer_description_en')}}</textarea>

        @if ($errors->has('footer_description_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('footer_description_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">وصف الموقع بالفوتر بالعربية: </label>
    <div class="col-lg-10{{ $errors->has('footer_description_ar') ? ' has-danger' : '' }}">
        <textarea name="footer_description_ar" class="form-control summernote " required>{{isset($setting) ? $setting->footer_description_ar :old('footer_description_ar')}}</textarea>

        @if ($errors->has('footer_description_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('footer_description_ar') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">البريد الاكترونى: </label>
    <div class="col-lg-10{{ $errors->has('email') ? ' has-danger' : '' }}">
        {!! Form::text('email',old('email'),['class'=>'form-control m-input','placeholder'=>'البريد الاكترونى']) !!}
        @if ($errors->has('email'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">رقم الهاتف: </label>
    <div class="col-lg-10{{ $errors->has('phone') ? ' has-danger' : '' }}">
        {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','placeholder'=>'رقم الهاتف']) !!}
        @if ($errors->has('phone'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">العنوان: </label>
    <div class="col-lg-10{{ $errors->has('address') ? ' has-danger' : '' }}">
        {!! Form::text('address',old('address'),['class'=>'form-control m-input','placeholder'=>'العنوان']) !!}
        @if ($errors->has('address'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">فيسبوك: </label>
    <div class="col-lg-10{{ $errors->has('facebook') ? ' has-danger' : '' }}">
        {!! Form::text('facebook',old('facebook'),['class'=>'form-control m-input','placeholder'=>'حساب فيسبوك']) !!}
        @if ($errors->has('facebook'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('facebook') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">تويتر: </label>
    <div class="col-lg-10{{ $errors->has('twitter') ? ' has-danger' : '' }}">
        {!! Form::text('twitter',old('twitter'),['class'=>'form-control m-input','placeholder'=>'حساب تويتر']) !!}
        @if ($errors->has('twitter'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('twitter') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">يوتيوب: </label>
    <div class="col-lg-10{{ $errors->has('youtube') ? ' has-danger' : '' }}">
        {!! Form::text('youtube',old('youtube'),['class'=>'form-control m-input','placeholder'=>'حساب يوتيوب']) !!}
        @if ($errors->has('youtube'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('youtube') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">انستجرام: </label>
    <div class="col-lg-10{{ $errors->has('insta') ? ' has-danger' : '' }}">
        {!! Form::text('insta',old('insta'),['class'=>'form-control m-input','placeholder'=>'حساب انستجرام']) !!}
        @if ($errors->has('insta'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('insta') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">سناب شات: </label>
    <div class="col-lg-10{{ $errors->has('snapchat') ? ' has-danger' : '' }}">
        {!! Form::text('snapchat',old('snapchat'),['class'=>'form-control m-input','placeholder'=>'حساب سناب شات']) !!}
        @if ($errors->has('snapchat'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('snapchat') }}</strong>
            </span>
        @endif
    </div>
</div>



<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">الصورة التعبيريه عنا  : </label>
    <div class="col-lg-10{{ $errors->has('expressive_image') ? ' has-danger' : '' }}">

        <input type="file" name="expressive_image"  class="form-control uploadinput">
        @if ($errors->has('expressive_image'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('expressive_image') }}</strong>
            </span>
        @endif
    </div>

</div>

@if(isset($setting) )
    <div class="col-12 text-center">
        <img src="{{$setting->expressive_image}}" style="max-width:120px;max-height:120px align="center">

    </div>
@endif
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">اللوجو : </label>
    <div class="col-lg-10{{ $errors->has('logo') ? ' has-danger' : '' }}">

        <input type="file" name="logo"  class="form-control uploadinput">
        @if ($errors->has('logo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('logo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($setting) )
    <div class="col-12 text-center">
        <img src="{{$setting->logo}}" style="max-width:120px;max-height:120px align="center">

    </div>
@endif



