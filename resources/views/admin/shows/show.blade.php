@extends('admin.layouts.app')

@section('title')
    عرض تفاصيل العرض
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسة</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/shows')}}" class="m-menu__link">
            <span class="m-menu__link-text">العروض</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">عرض تفاصيل العرض</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        عرض تفاصيل العرض
                    </h3>
                </div>
            </div>
            {{--<div style="margin-top:12px"><a href="/downloadFile/{{$patient->id}}" style="margin-bottom:20px" class="btn btn_primary btn btn-danger" ><i class=" fa fa-download"></i>{{trans('side.print_patient')}}</a></div>--}}
            {{--<div style="margin-top:12px"> <a href="#" title="{{trans('side.print')}}" class="greyscreen-print" onclick="window.print()"><i class="la la-print"></i></a>--}}
            </div>


        <div class="m-portlet__body">
        <!--begin::Form-->
        {!! Form::model($show,['route' => ['shows.show' , $show->id],'method'=> 'GET','class'=>'m-form m-form--fit m-form--label-align-right']) !!}
        {{--start patient data--}}
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">الفنان: </label>
        <div class="col-lg-10{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <input type="text" name="artist_id" value="{{$show->artist->name}}" class="form-control" disabled>
            @if ($errors->has('content_ar'))
                <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('content_ar') }}</strong>
            </span>
            @endif
        </div>

    </div>
    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">عنوان الفاعليه باللغة العربية: </label>
        <div class="col-lg-4{{ $errors->has('title_ar') ? ' has-danger' : '' }}">
            {!! Form::text('title_ar',old('title_ar'),['class'=>'form-control m-input','disabled','autofocus','placeholder'=> 'عنوان الفاعلية باللغة العربية' ]) !!}
            @if ($errors->has('title_ar'))
                <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_ar') }}</strong>
            </span>
            @endif
        </div>
        <label class="col-lg-2 col-form-label">عنوان الفاعلية باللغة الانجليزية : </label>
        <div class="col-lg-4{{ $errors->has('title_en') ? ' has-danger' : '' }}">
            {!! Form::text('title_en',old('title_en'),['class'=>'form-control m-input','disabled','placeholder'=>'عنوان الفاعلية باللغة الانجليزية']) !!}
            @if ($errors->has('title_en'))
                <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_en') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">وصف الفاعليه باللغة العربية: </label>
        <div class="col-lg-10{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <textarea name="content_ar" class="form-control summernote " disabled="true">{{isset($show) ? $show->content_ar :old('content_ar')}}</textarea>

            @if ($errors->has('content_ar'))
                <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('content_ar') }}</strong>
            </span>
            @endif
        </div>

    </div>
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">وصف الفاعلية باللغة الانجليزية: </label>
        <div class="col-lg-10{{ $errors->has('content_en') ? ' has-danger' : '' }}">
            <textarea name="content_en" class="form-control summernote " disabled="true">{{isset($show) ? $show->content_en :old('content_en')}}</textarea>

            @if ($errors->has('content_en'))
                <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('content_en') }}</strong>
            </span>
            @endif
        </div>

    </div>
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">تاريخ الفاعليه: </label>
        <div class="col-lg-10{{ $errors->has('date') ? ' has-danger' : '' }}">

            <input type="text" name="date" value="{{isset($show) ?$show->date: old('date') }}" class="form-control " disabled="true">
            @if ($errors->has('date'))
                <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('date') }}</strong>
            </span>
            @endif
        </div>

    </div>
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">الصوره الرئيسيه: </label>
        <div class="col-lg-10{{ $errors->has('date') ? ' has-danger' : '' }}">

            <img src="{{$show->main_image}}" style="max-width:120px;max-height:120px align="center">

        </div>

    </div>
    @if(isset($show) && count($show->images)>0)
        <label class="col-lg-2 col-form-label">صور وفيديوهات أخرى: </label>

        <div class='col-sm-8' style="text-align: center">
            @foreach($show->images as $item)
                @if(in_array($item->type,['jpeg','png','jpg','gif']))
                    <img src="{{$item->media}}" style="max-width:120px;max-height:120px " align="center">

                @else
                    <video width="120" height="120" controls>
                        <source src="{{ $item->media }}" type="video/mp4" style="max-width:120px;max-height:120px ">
                        Your browser does not support the video tag.
                    </video>
                @endif
            @endforeach
        </div>
    @endif
    </div>
    </div>


@endsection

