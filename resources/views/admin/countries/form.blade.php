<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">اسم الدوله بالعربيه: </label>
    <div class="col-lg-4{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم الدوله بالعربيه"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">اسم الدوله بالانجليزيه : </label>
    <div class="col-lg-5{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','placeholder'=>"اسم الدوله بالانجليزيه"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">
<label class="col-lg-2 col-form-label">نوع الدولة : </label>
<div class="col-lg-10{{ $errors->has('type') ? ' has-danger' : '' }}">
   <select name="type" class="form-control">
       <option value="" disabled selected>اختر نوع الدوله</option>
       <option value="1" {{isset($country) && $country->type==1?'selected':'' }}>عربية</option>
       <option value="2" {{isset($country) && $country->type==2?'selected':'' }}>غير عربية</option>
       <option value="3" {{isset($country) && $country->type==3?'selected':'' }}>محلى</option>

   </select>
    @if ($errors->has('type'))
        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('type') }}</strong>
            </span>
    @endif
</div>
</div>



