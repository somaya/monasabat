@extends('admin.layouts.app')
@section('title')
 عرض الدول
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item active-top-bar">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">الدول</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>

@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        الدول
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div><a href="{{route('countries.create')}}" style="margin-bottom:20px" class="btn btn_primary btn btn-danger" ><i class=" fa fa-edit"></i> إضافة دولة</a></div>


            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_country">
                <thead>
                <tr>
                    <th>#</th>
                    <th> الإسم باللغه العربيه</th>
                    <th> الإسم باللغه الانجليزيه</th>
                    <th>الأدوات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($countries as $index=>$country)
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$country->name_ar}}</td>
                        <td>{{$country->name_en}}</td>
                        <td>
                            <a  title="تعديل" href="/webadmin/countries/{{$country->id}}/edit" ><i class="fa fa-edit"></i></a>
                            <form class="inline-form-style"
                                  action="/webadmin/countries/{{ $country->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete" />
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/countrcountriess!!}--}}
@endsection
