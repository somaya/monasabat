@extends('admin.layouts.app')
@section('title')
   الخدمات
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسيه</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/services')}}" class="m-menu__link">
            <span class="m-menu__link-text">الخدمات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        الخدمات
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div><a href="{{route('services.create')}}" style="margin-bottom:20px" class="btn btn_primary btn btn-danger" ><i class=" fa fa-edit"></i> إضافة خدمة</a></div>


            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_testArea">
                <thead>
                <tr>
                    <th>#</th>
                    <th> عنوان الخدمة باللغة العربية</th>
                    <th> عنوان الخدمه باللغة الانجليزية</th>
                    <th>الأدوات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($services as $index=> $service)

                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$service->title_ar}} </td>
                        <td>{{$service->title_en}} </td>
                        <td>
                            <a  title="عرض"   href="/webadmin/services/{{$service->id}}" ><i class="fa fa-eye"></i></a>
                            <a  title="تعديل" href="/webadmin/services/{{$service->id}}/edit" ><i class="fa fa-edit"></i></a>
                            <form class="inline-form-style" 
                                  action="/webadmin/services/{{ $service->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete" />
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}
@endsection
