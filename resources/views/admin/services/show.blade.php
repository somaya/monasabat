@extends('admin.layouts.app')

@section('title')
    عرض تفاصيل خدمة
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسة</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/services')}}" class="m-menu__link">
            <span class="m-menu__link-text">الخدمات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">عرض تفاصيل الخدمة</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                         عرض تفاصيل الخدمة
                    </h3>
                </div>
            </div>
            {{--<div style="margin-top:12px"><a href="/downloadFile/{{$patient->id}}" style="margin-bottom:20px" class="btn btn_primary btn btn-danger" ><i class=" fa fa-download"></i>{{trans('side.print_patient')}}</a></div>--}}
            {{--<div style="margin-top:12px"> <a href="#" title="{{trans('side.print')}}" class="greyscreen-print" onclick="window.print()"><i class="la la-print"></i></a>--}}
            </div>


        <div class="m-portlet__body">

        <!--begin::Form-->
        {!! Form::model($service,['route' => ['services.show' , $service->id],'method'=> 'GET','class'=>'m-form m-form--fit m-form--label-align-right']) !!}
        {{--start patient data--}}

    <div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label">عنوان الخدمة باللغة العربية: </label>
        <div class="col-lg-4{{ $errors->has('title_ar') ? ' has-danger' : '' }}">
            {!! Form::text('title_ar',old('title_ar'),['class'=>'form-control m-input','disabled','autofocus','placeholder'=> 'عنوان الفاعلية باللغة العربية' ]) !!}
            @if ($errors->has('title_ar'))
                <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_ar') }}</strong>
            </span>
            @endif
        </div>
        <label class="col-lg-2 col-form-label">عنوان الخدمة باللغة الانجليزية : </label>
        <div class="col-lg-4{{ $errors->has('title_en') ? ' has-danger' : '' }}">
            {!! Form::text('title_en',old('title_en'),['class'=>'form-control m-input','disabled','placeholder'=>'عنوان الفاعلية باللغة الانجليزية']) !!}
            @if ($errors->has('title_en'))
                <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_en') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">وصف الخدمة باللغة العربية: </label>
        <div class="col-lg-10{{ $errors->has('content_ar') ? ' has-danger' : '' }}">
            <textarea name="description_ar" class="form-control summernote " disabled="true">{{isset($service) ? $service->description_ar :old('description_ar')}}</textarea>

            @if ($errors->has('description_ar'))
                <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('description_ar') }}</strong>
            </span>
            @endif
        </div>

    </div>
    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">وصف الخدمة باللغة الانجليزية: </label>
        <div class="col-lg-10{{ $errors->has('description_en') ? ' has-danger' : '' }}">
            <textarea name="description_en" class="form-control  " disabled="true">{{isset($service) ? $service->description_en :old('description_en')}}</textarea>

            @if ($errors->has('description_en'))
                <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('description_en') }}</strong>
            </span>
            @endif
        </div>

    </div>

    <div class="form-group m-form__group row">

        <label class="col-lg-2 col-form-label">الصوره : </label>
        <div class="col-lg-10{{ $errors->has('date') ? ' has-danger' : '' }}">

            <img src="{{$service->image}}" style="max-width:120px;max-height:120px align="center">

        </div>

    </div>
    </div>
    </div>



@endsection

