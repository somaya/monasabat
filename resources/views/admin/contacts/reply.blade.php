@extends('admin.layouts.app')

@section('title')
    الرد على  الرسالة
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسة</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/contacts')}}" class="m-menu__link">
            <span class="m-menu__link-text">رسائل اتصل بنا</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text"> الرد على الرسالة </span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        الرد على الرسالة
                    </h3>
                </div>
            </div>
            {{--<div style="margin-top:12px"><a href="/downloadFile/{{$patient->id}}" style="margin-bottom:20px" class="btn btn_primary btn btn-danger" ><i class=" fa fa-download"></i>{{trans('side.print_patient')}}</a></div>--}}
            {{--<div style="margin-top:12px"> <a href="#" title="{{trans('side.print')}}" class="greyscreen-print" onclick="window.print()"><i class="la la-print"></i></a>--}}
        </div>


        <div class="m-portlet__body">

            <!--begin::Form-->
            <form action="/webadmin/contact/{{$contact->id}}/sendreply" method="post">
                @csrf

            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">الرد: </label>
                <div class="col-lg-10{{ $errors->has('message') ? ' has-danger' : '' }}">
                    <textarea name="message" class="form-control  " >{{old('message')}}</textarea>

                    @if ($errors->has('message'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('message') }}</strong>
            </span>
                    @endif
                </div>

            </div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-success">إرسال</button>
                </div>

            </form>

        </div>
    </div>


@endsection

